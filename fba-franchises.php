<?php
/**
 * Plugin Name:       FBA Franchise Search
 * Plugin URI:        http://wpconsole.com
 * Git URI:           https://bitbucket.org/ivanlopezdev/fba-franchise
 * Description:       This plugin integrates your website with the Franchise Broker Association.
 * Version:           3.0.12
 * Author:            Franchise Broker Association
 * Author URI:        http://franchiseba.com
 * License:           GPL-2.0+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'FBA_API_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'FBA_API_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

if ( ! class_exists( 'JSONH' ) ) {
	include_once FBA_API_PLUGIN_DIR . 'includes/api/JSONH.class.php';
}

if ( file_exists( FBA_API_PLUGIN_DIR . 'includes/cmb2/init.php' ) ) {
	require_once FBA_API_PLUGIN_DIR . 'includes/cmb2/init.php';
}

include_once FBA_API_PLUGIN_DIR . 'includes/api/class-fba-api.php';
include_once FBA_API_PLUGIN_DIR . 'includes/widgets/achivements.php';
include_once FBA_API_PLUGIN_DIR . 'includes/widgets/franchises.php';
include_once FBA_API_PLUGIN_DIR . 'includes/widgets/search-form.php';
include_once FBA_API_PLUGIN_DIR . 'includes/shortcodes/achivements.php';
include_once FBA_API_PLUGIN_DIR . 'includes/shortcodes/franchises.php';
include_once FBA_API_PLUGIN_DIR . 'includes/shortcodes/search-form.php';
include_once FBA_API_PLUGIN_DIR . 'includes/shortcodes/questionnaire.php';
include_once FBA_API_PLUGIN_DIR . 'includes/shortcodes/client.php';
include_once FBA_API_PLUGIN_DIR . 'includes/template-helpers.php';
include_once FBA_API_PLUGIN_DIR . 'includes/search-form.php';
include_once FBA_API_PLUGIN_DIR . 'includes/sidebars.php';
include_once FBA_API_PLUGIN_DIR . 'includes/templates/franchise-details.php';
include_once FBA_API_PLUGIN_DIR . 'includes/ajax.php';
include_once FBA_API_PLUGIN_DIR . 'includes/class-fba-franchises.php';

/*----------------------------------------------------------------------------*
 * Globals
 *----------------------------------------------------------------------------*/

$franchise;
$franchise_args;
$fba_settings;
$fba_api_form;
$fba_check_cookie;
$fba_show_details;
$fba_show_message;

/*----------------------------------------------------------------------------*
 * Public Update Checker
 *----------------------------------------------------------------------------*/

include_once FBA_API_PLUGIN_DIR . 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/ivanlopezdev/fba-franchise',
	__FILE__,
	'fba-franchise'
);

$myUpdateChecker->setBranch( 'master' );

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

register_activation_hook( __FILE__, array( 'FBA_Franchise', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'FBA_Franchise', 'deactivate' ) );
add_action( 'plugins_loaded', array( 'FBA_Franchise', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
	include_once FBA_API_PLUGIN_DIR . 'includes/admin/class-fba-franchises-admin.php';
	add_action( 'plugins_loaded', array( 'FBA_Franchise_Admin', 'get_instance' ) );
}

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/


/* function git_plugin_update() {
	if ( is_admin() && ! class_exists( 'GPU_Controller' ) ) {
		include_once FBA_API_PLUGIN_DIR . 'includes/git-plugin-updates/git-plugin-updates.php';
		add_action( 'plugins_loaded', 'GPU_Controller::get_instance', 20 );
	}
}

add_action( 'plugins_loaded', 'git_plugin_update' ); */


add_action( 'gform_loaded', array( 'FBA_AddOn_Bootstrap', 'load' ), 5 );

class FBA_AddOn_Bootstrap {

	public static function load() {

		if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
			return;
		}


		require_once( FBA_API_PLUGIN_DIR . 'includes/gravityforms.php' );
		require_once( FBA_API_PLUGIN_DIR . 'includes/gravityformsfield.php' );

		GFAddOn::register( 'FBAAddOn' );
	}

}

// Clear cache via browser
if( isset( $_GET['fba-cache'] ) && 'clear' ===  $_GET['fba-cache'] ) {
	wp_cache_flush();
	add_filter('wp_head', function(){
		echo 'Cache wa cleared';
	});
}