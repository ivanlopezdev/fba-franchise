<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function fba_franchises( $atts ) {
	$atts = extract( shortcode_atts( array( 'display'  => '',
	                                        'category' => '',
	                                        'ids'      => '',
	                                        'featured' => '',
	                                        'count'    => ''
			), $atts ) );


	$active = get_transient( 'fba_account_status' );

	$content = "<div class='fba-franchise'>";

	if ( ! $active ) {
		$status = FBA_API::get_status();
		$active = $status->broker;
		set_transient( 'fba_account_status', $active, 86400 );
	}

	if ( $active ) {

		$display        = ( isset( $display ) ) ? $display : "name";
		$category       = ( isset( $category ) ) ? $category : "";
		$ids            = ( isset( $ids ) ) ? $ids : "";
		$featured       = ( isset( $featured ) ) ? $featured : "";
		$featured_count = ( isset( $count ) ) ? $count : 5;
		$count          = 0;
		if ( ! empty( $featured ) ) {
			$franchises = FBA_API::get_featured();
		} elseif ( isset( $category ) && $category != "" ) {
			$franchises = FBA_API::get_search_results( '&cid=' . $category );
		} else {
			$franchises = FBA_API::get_franchise_by_id( $ids );
		}

		foreach ( $franchises as $franchise ) {
			$count ++;
			if ( $count <= $featured_count ) {
				if ( $display == "name" ) {
					$content .= "<p><a href='" . home_url( '/franchise/' . $franchise->slug ) . "'>" . esc_attr( $franchise->name ) . "</a></p>";
				} else {
					$content .= "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "'><img src='" . esc_attr( $franchise->logo ) . "'></a>";
				}
			}
		}

	} else {
		$content .= "<p>Your Franchise Brokers Association account is currently not active please <a href='http://franchiseba.com/' target='_blank'>contact us</a> to activate your account.";
	}

	$content .= '</div>';

	return $content;
}

add_shortcode( 'franchises', 'fba_franchises' );