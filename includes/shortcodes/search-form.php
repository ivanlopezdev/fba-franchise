<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function fba_franchise_form( $atts ) {
	$atts = extract( shortcode_atts( array( 'default' => 'values' ), $atts ) );

	ob_start();
	?>


	<div class="recent_search container_12">
		<div itemprop="text">
			<?php 
				if( isset( $_GET['category'] ) ){
					fba_search_form( home_url( 'franchise-search' ) );
				} else {
					fba_search_form( home_url( 'franchise-search' ), false, false );
				}
				 ?>
		</div>
	</div>
	<?php if ( get_query_var( 'category' ) ||  get_query_var( 'fran-order' ) ) : ?>
		<div class="search-results container_12">
			
			<?php
			global $franchise_args;
			$results = FBA_API::get_search_results( $franchise_args );  ?>

			<?php if ( $results ): ?>

				<div class="result gridRow">
					<?php fba_search_header() ?>
				</div>

				<?php foreach ( $results as $result ): ?>
					<div class="result gridRow">
						<div class="result-image grid_3">
							<?php fba_franchise_listing_logo( $result ) ?>
						</div>
						<div class="result-content grid_6">
							<?php fba_franchise_listing_details( $result ) ?>
						</div>
						<div class="result-image grid_3">
							<?php fba_franchise_data( $result ) ?>
						</div>
					</div>
				<?php endforeach ?>

			<?php else: ?>
				<div class="result-error">
					<p>
						<strong>No franchises matched with your selected search criteria. You may want to limit your selections to get better results</strong>.
					</p>

					<p>
						<strong>OR</strong> take a look at some of these exciting Franchise Business Opportunities that other business owners are taking a look at right now:
					</p>

					<div class='fba-franchise'>
						<?php
						$featured_count = 5;
						$count          = 0;
						$franchises     = FBA_API::get_featured();
						shuffle( $franchises );
						$content        = '';
						foreach ( $franchises as $franchise ) {
							$count ++;
							if ( $count <= $featured_count ) {
								$content .= "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "'><img src='" . esc_attr( $franchise->logo ) . "'></a>";
							}
						}

						echo $content;
						?>
					</div>
				</div>
			<?php endif ?>

			<div style="clear:both"></div>
		</div>

		<div style="display: none">
			<div id="request-information-modal">
				<h4>Request More Information on <span class="franchise-name"></span></h4>
				<?php $fba_settings = (array) get_option( 'fba_setting' ); ?>
				<?php if( ! empty( $fba_settings['request_information'] ) ): ?>
					<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['request_information'] ?>">
						<?php if(function_exists('gravity_form')){
							gravity_form( $fba_settings['request_information'], false, false, false, '', true );
						} ?>
					</div>
				<?php else: ?>
					<div class="form_message"></div>
					<div class="grid_5">
						<form action="" method="post" name="search_information" id="fba-search-request">
							<div class="form_field">
								<input type="text" name="first_name" placeholder="First Name">
							</div>
							<div class="form_field">
								<input type="text" name="last_name" placeholder="Last Name">
							</div>
							<div class="form_field">
								<input type="text" name="email" placeholder="Email">
							</div>
							<div class="form_field">
								<input type="text" name="phone" placeholder="Phone Number">
							</div>
							<div class="form_field">
								<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
							</div>
							<div class="btn_field">
								<input type="hidden"  name="action_type" value="franchise">
								<input type="submit" name="submit" value="Get More Info!">
							</div>

						</form>
					</div>
				<?php endif; ?>
				<div class="grid_7">
					<?php dynamic_sidebar( 'franchise-search-form' ); ?>
				</div>

				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			</div>
		</div>

		<div style="display: none">
			<div id="territory-check-modal">
				<h4>Territory Check on <span class="franchise-name"></span></h4>
				<?php if( ! empty( $fba_settings['territory_check'] ) ): ?>
					<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['territory_check'] ?>">
						<?php if(function_exists('gravity_form')){
							gravity_form( $fba_settings['territory_check'], false, false, false, '', true );
						} ?>
					</div>
				<?php else: ?>
					<div class="form_message"></div>
					<form action="" method="post" name="territory_information" id="fba-territory-request">

						<div class="grid_5">
							<div class="form_field">
								<input type="text" name="first_name" placeholder="First Name">
							</div>
							<div class="form_field">
								<input type="text" name="last_name" placeholder="Last Name">
							</div>
							<div class="form_field">
								<input type="text" name="email" placeholder="Email">
							</div>
							<div class="form_field">
								<input type="text" name="phone" placeholder="Phone Number">
							</div>
							<div class="form_field">
								<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
							</div>
							<div class="btn_field">
								<input type="hidden" name="action_type" value="territory">
								<input type="submit" name="submit" value="Get More Info!">
							</div>

						</div>

						<div class="grid_7">
							<div class="form_field">
								<textarea name="territories" cols="30" rows="2" placeholder="What territories are your interested in?"></textarea>
							</div>
							<div class="form_field">
								<input type="text" name="timeframe" id="timeframe" placeholder="What is your timeframe to purchase?">
							</div>
							<div class="form_field">
								<input type="text" name="invest" id="invest" placeholder="How much do you have to invest?">
							</div>
							<div class="form_field">
								<strong>What is the best time to call you:</strong> <br />
								<input type="radio" name="time" value="Morning"> Morning
								<input type="radio" name="time" value="Afternoon"> Afternoon
								<input type="radio" name="time" value="Evenings"> Evenings
							</div>
						</div>
					</form>
				<?php endif; ?>

				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			</div>
		</div>

		<?php fba_my_list(); ?>
	<?php else: ?>

		<?php if ( isset( $_COOKIE['fba-franchises']) && !empty( $_COOKIE['fba-franchises'] ) ): ?>
			<div class='fba-search-featured-franchise'>
				<h2>Recomended Franchises For You</h2>
				<div class="owl-carousel">
					<?php
					$recomended = json_decode(stripslashes( $_COOKIE['fba-franchises'] ) );
					$categories = array();
					$franchises = array();
					foreach ($recomended as $franchise ) {
						$data = explode('::', $franchise );
						$cat = explode(',', $data[2] );
						$categories = array_merge( $categories, $cat );
					}
					$categories = array_unique($categories);
					shuffle( $categories );
					if ( isset( $categories[0] ) ) {
						$catID = array_search( $categories[0], FBA_API::get_categories() );
						$args = '&cid=' . $catID ;
						$franchises = FBA_API::get_search_results( $args );
					}
					$featured_count = 10;
					$count          = 0;
					$content        = '';
					foreach ( $franchises as $franchise ) {
						$count ++;
						if ( $count <= $featured_count ) {
							$content .= "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "' class='item' ><img src='" . esc_attr( $franchise->logo ) . "'></a>";
						}
					}
					echo $content;
					?>
				</div>
			</div>
		<?php endif ?>

		<div class='fba-search-featured-franchise'>
			<h2>Featured Franchises</h2>
			<div class="owl-carousel">
			<?php
			$featured_count = 10;
			$count          = 0;
			$franchises     = FBA_API::get_featured();
			shuffle( $franchises );
			$content        = '';
			foreach ( $franchises as $franchise ) {
				$count ++;
				if ( $count <= $featured_count ) {
					$content .= "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "' class='item'><img src='" . esc_attr( $franchise->logo ) . "'></a>";
				}
			}
			echo $content;
			?>
			</div>
		</div>
	<?php endif; ?>

	<?php
	$content = ob_get_clean();

	return $content;
}

add_shortcode( 'franchise-search', 'fba_franchise_form' );