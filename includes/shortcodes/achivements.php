<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function fba_achievements( $atts ) {
	$atts = extract( shortcode_atts( array( 'default' => 'values' ), $atts ) );

	$active = get_transient( 'fba_account_status' );


	if ( ! $active ) {
		$status = FBA_API::get_status();
		$active = $status->broker;
		set_transient( 'fba_account_status', $active, 86400 );
	}

	$content = "<div class='fba-achievements' >";
	if ( $active ) {
		$achivements = FBA_API::get_achivments();

		foreach ( $achivements->broker as $achivement ) {
			$content .= '<img src="' . $achivement . '">';
		}
	} else {
		$content .= "<p>Your Franchise Brokers Association account is currently not active please <a href='http://franchiseba.com/' target='_blank'>contact us</a> to activate your account.";
	}
	$content .= "</div>";

	return $content;
}

add_shortcode( 'achievements', 'fba_achievements' );