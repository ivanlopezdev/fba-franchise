<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function fba_client( $atts ) {
	$atts = extract( shortcode_atts( array( 'default' => 'values' ), $atts ) );

	if( empty( $_GET['client'] ) ){
		return '<p>No client was provided</p>';
	}

	ob_start();
	?>

	<div id="fba-loading" style="text-align:center; padding:20px;">
		<img src="<?php echo esc_url( FBA_API_PLUGIN_URL ); ?>/assets/images/loading.gif" width="243" height="189">
	</div>

	<iframe id="fba-iframe" frameborder="0" src="https://fbamembers.com/public-client/<?php echo $_GET['client']; ?>/" style="overflow:hidden; overflow-x:hidden;overflow-y:hidden;" width="100%" height="0"></iframe>

	<script>
		var iframeForm = document.getElementById( 'fba-iframe' );
		var iframeLoader = document.getElementById( 'fba-loading' );
		iframeForm.onload = function () {
			iframeLoader.style.display = 'none';
		};

		window.addEventListener( "message", function ( event ) {
			iframeLoader.style.display = 'none';
			iframeForm.height = event.data.height + 60;
		}, false );
	</script>


	<?php
	$content = ob_get_clean();

	return $content;
}

add_shortcode( 'fba_client', 'fba_client' );