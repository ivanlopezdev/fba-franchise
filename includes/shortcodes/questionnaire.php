<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function fba_franchise_questionnaire( $atts ) {
	$atts = extract( shortcode_atts( array( 'default' => 'values' ), $atts ) );

	ob_start();
	?>

	<form action="" id="fba-questionnaire" class="fba-questionnaire" name="franchise_questionnaire">
		<div class="container_12">
			<div class="gridRow">
				<div class="grid_12">
					<p>Fill out our simple questionnaire to get an instant list or recommended concepts based on your answers. </p>
				</div>
			</div>

			<div class="form_message_questionnaire" style="color:#a50606"></div>

			<div class="form-fields">
				<div class="gridRow">
					<div class="grid_6">
						<label for="_fba_first_name">First Name*</label>
						<input type="text" id="_fba_first_name" name="_fba_first_name" placeholder="First Name">
					</div>
					<div class="grid_6">
						<label for="_fba_last_name">Last Name*</label>
						<input type="text" id="_fba_last_name" name="_fba_last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="gridRow">
					<div class="grid_6">
						<label for="_fba_phone">Phone Number*</label>
						<input type="text" id="_fba_phone" name="_fba_phone" placeholder="Phone Number">
					</div>
					<div class="grid_6">
						<label for="_fba_email">Email*</label>
						<input type="email" id="_fba_email" name="_fba_email" placeholder="Email">
					</div>
				</div>

				<div class="gridRow">
					<div class="grid_4">
						<label for="_fba_city">City*</label>
						<input type="text" id="_fba_city" name="_fba_city" placeholder="City">
					</div>
					<div class="grid_4">
						<label for="_fba_state">State*</label>
						<select name="_fba_state" id="_fba_state">
							<?php foreach ( FBA_API::get_states() as $key => $value ): ?>
								<option value="<?php echo $key ?>"><?php echo $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="grid_4">
						<label for="_fba_zip">Zipcode*</label>
						<input type="text" id="_fba_zip" name="_fba_zip" placeholder="Zipcode">
					</div>
				</div>

				<div class="gridRow">
					<div class="grid_4">
						<label for="_fba_invest">Max Investment*</label>
						<input type="text" data-type='number' id="_fba_invest" name="_fba_invest" placeholder="Max Investment">
					</div>
					<div class="grid_4">
						<label for="_fba_fba_networth">Net Worth</label>
						<input type="text" data-type='number' id="_fba_fba_networth" name="_fba_fba_networth" placeholder="Net Worth">
					</div>
					<div class="grid_4">
						<label for="_fba_liquid_cash">Liquid Cash</label>
						<input type="text" data-type='number' id="_fba_liquid_cash" name="_fba_liquid_cash" placeholder="Liquid Cash">
					</div>
				</div>

				<div class="gridRow">
					<div class="grid_6">
						<label for="_fba_category_rating">Concept Categories</label>
						<select name="_fba_category_rating[]" id="_fba_category_rating" multiple class="fba-select2">
							<?php foreach ( FBA_API::get_categories() as $category ): ?>
								<option value="<?php echo $category->slug ?>"><?php echo $category->name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="grid_6">
						<label for="validator">Are you human?*</label>
						<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
					</div>
				</div>

				<div class="gridRow">
					<div class="grid_12">
						<input type="submit" value="Submit">
					</div>
				</div>
			</div>

			<div class="search-results container_12" style="display: none">
				<div class="gridRow">
					<div class="grid_12">
						<h2>Below is a small list of franchises that fit based on the info you provided. To get results that are a better match for you schedule a call with us.</h2>
					</div>
				</div>
				<div class="result gridRow loading">
					<p style="text-align: center">Loading Recommended Concepts... <br>
						<svg xmlns="http://www.w3.org/2000/svg" width="38" height="38" viewBox="0 0 38 38" stroke="#ddd" style="margin-top: 15px">
							<g fill="none" fill-rule="evenodd">
								<g transform="translate(1 1)" stroke-width="2">
									<circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
									<path d="M36 18c0-9.94-8.06-18-18-18" transform="rotate(78.4652 18 18)">
										<animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/>
									</path>
								</g>
							</g>
						</svg>
					</p>
				</div>
				<div class="result gridRow">

				</div>
			</div>
		</div>
	</form>

	<div style="display: none">
		<div id="request-information-modal">
			<h4>Request More Information on <span class="franchise-name"></span></h4>
			<?php $fba_settings = (array) get_option( 'fba_setting' ); ?>
			<?php if( ! empty( $fba_settings['request_information'] ) ): ?>
				<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['request_information'] ?>">
					<?php if(function_exists('gravity_form')){
						gravity_form( $fba_settings['request_information'], false, false, false, '', true );
					} ?>
				</div>
			<?php else: ?>
				<div class="form_message"></div>
				<div class="grid_5">
					<form action="" method="post" name="search_information" id="fba-search-request">
						<div class="form_field">
							<input type="text" name="first_name" placeholder="First Name">
						</div>
						<div class="form_field">
							<input type="text" name="last_name" placeholder="Last Name">
						</div>
						<div class="form_field">
							<input type="text" name="email" placeholder="Email">
						</div>
						<div class="form_field">
							<input type="text" name="phone" placeholder="Phone Number">
						</div>
						<div class="form_field">
							<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
						</div>
						<div class="btn_field">
							<input type="hidden"  name="action_type" value="franchise">
							<input type="submit" name="submit" value="Get More Info!">
						</div>

					</form>
				</div>
			<?php endif; ?>

			<div class="grid_7">
				<?php dynamic_sidebar( 'franchise-search-form' ); ?>
			</div>

			<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
		</div>
	</div>

	<div style="display: none">
		<div id="territory-check-modal">
			<h4>Territory Check on <span class="franchise-name"></span></h4>
			<div class="form_message"></div>
			<form action="" method="post" name="territory_information" id="fba-territory-request">

				<div class="grid_5">
					<div class="form_field">
						<input type="text" name="first_name" placeholder="First Name">
					</div>
					<div class="form_field">
						<input type="text" name="last_name" placeholder="Last Name">
					</div>
					<div class="form_field">
						<input type="text" name="email" placeholder="Email">
					</div>
					<div class="form_field">
						<input type="text" name="phone" placeholder="Phone Number">
					</div>
					<div class="form_field">
						<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
					</div>
					<div class="btn_field">
						<input type="hidden" name="action_type" value="territory">
						<input type="submit" name="submit" value="Get More Info!">
					</div>

				</div>

				<div class="grid_7">
					<div class="form_field">
						<textarea name="territories" cols="30" rows="2" placeholder="What territories are your interested in?"></textarea>
					</div>
					<div class="form_field">
						<input type="text" name="timeframe" id="timeframe" placeholder="What is your timeframe to purchase?">
					</div>
					<div class="form_field">
						<input type="text" name="invest" id="invest" placeholder="How much do you have to invest?">
					</div>
					<div class="form_field">
						<strong>What is the best time to call you:</strong> <br />
						<input type="radio" name="time" value="Morning"> Morning
						<input type="radio" name="time" value="Afternoon"> Afternoon
						<input type="radio" name="time" value="Evenings"> Evenings
					</div>
				</div>
			</form>

			<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
		</div>
	</div>
	<?php
	$content = ob_get_clean();

	return $content;
}

add_shortcode( 'franchise-questionnaire', 'fba_franchise_questionnaire' );