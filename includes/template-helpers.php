<?php

function fba_franchise_listing_logo( $result ) {
	?>
	<a href="<?php echo home_url( '/franchise/' . $result->slug ); ?>" title="<?php echo esc_attr( $result->name ); ?>">
		<img src="<?php echo $result->logo; ?>">
	</a>
	<?php if( isset( $result->worldclass ) && $result->worldclass[0] == 'World Class Franchise'): ?>
		<img src="<?php echo FBA_API_PLUGIN_URL . 'assets/images/worldclass-logo.png' ?>">
	<?php
	endif;
}

function fba_franchise_listing_details( $result ) {
	?>
	<a href="<?php echo home_url( '/franchise/' . $result->slug ); ?>" title="<?php echo esc_attr( $result->name ); ?>">
		<h3><?php echo esc_attr( $result->name ); ?></h3>
	</a>
	<?php if( isset($result->featured) && $result->featured == 'on'): ?>
		<p style="margin-bottom: 15px;"><strong ><img src="<?php echo FBA_API_PLUGIN_URL . 'assets/images/gold-star.png' ?>" style="vertical-align:top"> Recommended Franchise</strong></p>
	<?php endif; ?>
	<p><?php echo esc_attr( $result->description ); ?></p>
	<p>
		<a href="<?php echo home_url( '/franchise/' . $result->slug ); ?>" title="<?php echo esc_attr( $result->name ); ?>">Read More...</a>
	</p>
<?php
}

function fba_franchise_data( $result ) {
	?>
	<div class="basic-franchise-info">
		<h4>Basic Franchise Info</h4>
		<?php if ( ! empty( $result->average_investment ) && 'N/A' !== $result->average_investment ): ?>
			<p><strong>Average Investment: </strong>$<?php echo esc_attr( $result->average_investment ); ?></p>
		<?php else: ?>
			<p><strong>Average Investment: </strong> N/A</p>
		<?php endif; ?>
		<?php if ( ! empty( $result->minimum_investment ) && 'N/A' !== $result->minimum_investment ): ?>
			<p><strong>Minimum Investment: </strong>$<?php echo esc_attr( $result->minimum_investment ); ?></p>
		<?php else: ?>
			<p><strong>Minimum Investment: </strong> N/A</p>
		<?php endif; ?>
		<?php if ( isset( $result->area_master ) && 'Area/Master Developer' === $result->area_master ): ?>
			<p><strong>Area/Master Developer: </strong> Yes</p>
		<?php endif; ?>
		<p>
			<a href="#request-information-modal" class="request-information" data-franchise="<?php echo  $result->id ?>::<?php echo  $result->name ?>">Request Information</a>
			<a href="#territory-check-modal" class="check-territory" data-franchise="<?php echo  $result->id ?>::<?php echo  $result->name ?>">Check Territory</a>
		</p>
		<p>
			<input type="checkbox" data-franchise="<?php echo  $result->id ?>::<?php echo  $result->name ?>::<?php echo  str_replace('|', ', ', $result->category )?>::<?php echo  $result->average_investment ?>" class="mylist" id="list-<?php echo  $result->id ?>" /> <label for="list-<?php echo  $result->id ?>">Add to My Franchise List</label>
		</p>
	</div>
<?php
}


function fba_franchise_title() {
	global $franchise;

	return esc_attr( $franchise->name );
}


function fba_franchise_logo() {
	global $franchise;
	?>
	<img src="<?php echo $franchise->logo; ?>">
<?php
}

function fba_franchise_details() {
	global $franchise, $fba_show_message, $fba_show_details, $fba_api_form;
	?>
	<?php if ( $fba_show_message ): ?>
		<h4 class="sucess">Your information has been submitted successfully.</h4>
	<?php endif ?>

	<?php if ( $fba_show_details ): ?>
		<p><b>Category:</b> <?php echo esc_attr( $franchise->category ); ?></p>
		<p><b>Min. Investment:</b> <?php echo esc_attr( $franchise->minimum_investment ); ?></p>
		<p><b>Min. Liquidity:</b> <?php echo esc_attr( $franchise->liquidity ); ?></p>
		<p><b>Years in Business:</b> <?php echo esc_attr( $franchise->year_established ); ?></p>
		<p><b>Open Units:</b> <?php echo esc_attr( $franchise->franchised_units ); ?></p>
		<p><b>In-House Financing:</b> <?php echo esc_attr( $franchise->financial_assistance ); ?></p>
		<p><b>Lead Assist:</b> <?php echo esc_attr( $franchise->lead_assistance ); ?></p>
		<p><b>Coaching:</b> <?php echo esc_attr( $franchise->financial_assistance ); ?></p>
	<?php endif ?>

	<?php if ( isset( $fba_api_form ) && $fba_api_form && ! $fba_show_details ): ?>
		<div class="form_message"></div>

		<form action="" method="post" name="more_information">
			<div class="form_field">
				<input type="text" name="first_name" placeholder="First Name">
			</div>
			<div class="form_field">
				<input type="text" name="last_name" placeholder="Last Name">
			</div>
			<div class="form_field">
				<input type="text" name="email" placeholder="Email">
			</div>
			<div class="form_field">
				<input type="text" name="phone" placeholder="Phone Number">
			</div>
			<div class="form_field">
				<input type="hidden" name="franchise" value="<?php echo $franchise->name; ?>">
				<textarea name="comment" cols="30" rows="10" placeholder="Comments and Questions"></textarea>
			</div>
			<div class="form_field">
				<input type="text" name="validator" id="validator" placeholder="Are you human?">
			</div>
			<div class="btn_field">
				<input type="submit" name="submit" value="Get More Info!">
			</div>

		</form>
	<?php endif ?>
<?php
}

function fba_franchise_description() {
	global $franchise;
	echo apply_filters( 'the_content', $franchise->public_description );
}

function fba_search_header() {
	?>
	<div class="result-image grid_12" style="margin-top: 20px; margin-bottom: 0px; ">
		<h4>Displaying Franchises in <?php echo esc_html( isset( $_GET['category'] ) ? $_GET['category'] : 'All Categories' ); ?></h4>

		<p>NOTE: Search results are a sample, and do not represent the complete list of franchises that we represent.
			<strong><a href="<?php echo home_url( '/contact/' ); ?>">Contact us</a> for a free consultation.</strong>
		</p>
	</div>
<?php
}

function fba_my_list(){
	?>
	<div style="display: none">
		<div id="franchise-list-modal">
			<h4>My Franchise List</h4>

			<p>The list of your selected franchises is below. After you are finished with your list, submit the form at the bottom to gain access to much more information about these opportunities. This list will also help us to better understand your investment goals and interests.</p>

			<div id="my-list">
				<header>
					<div class="action"></div>
					<div class="concept-name">Franchise Name</div>
					<div class="category">Category</div>
					<div class="avg-investment">Avg. Investment</div>
				</header>
				<div class="franchises-list">
				</div>
			</div>

			<h4>Request Information on These Franchises</h4>
			<div class="no_grid_margin grid_2">
				<img src="<?php echo FBA_API_PLUGIN_URL . '/assets/images/list.png' ?>">
			</div>
			<div class="no_grid_margin grid_10">
				<p>After submitting the information below you will be contacted by one of our representatives to offer you a free consultation. This consultation will look to better understand your personal and business goals so that we may better assist you in discovering the best franchise opportunity for you</p>
			</div>

			<div class="clearfix"></div>
			<?php $fba_settings = (array) get_option( 'fba_setting' ); ?>
			<?php if( ! empty( $fba_settings['territory_check'] ) ): ?>
				<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['territory_check'] ?>">
					<?php if(function_exists('gravity_form')){
						gravity_form( $fba_settings['territory_check'], false, false, false, '', true );
					} ?>
				</div>
				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			<?php else: ?>
				<div class="form_message"></div>

				<form action="" method="post" name="list_information" id="fba-list-request">

					<div class="grid_5">
						<div class="form_field">
							<input type="text" name="first_name" placeholder="First Name">
						</div>
						<div class="form_field">
							<input type="text" name="last_name" placeholder="Last Name">
						</div>
						<div class="form_field">
							<input type="text" name="email" placeholder="Email">
						</div>
						<div class="form_field">
							<input type="text" name="phone" placeholder="Phone Number">
						</div>
						<div class="form_field">
							<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
						</div>
						<div class="btn_field">
							<input type="hidden"  name="action_type" value="list">
							<input type="submit" name="submit" value="Get More Info!">
						</div>

					</div>

					<div class="grid_7">
						<div class="form_field">
							<textarea name="territories" cols="30" rows="2" placeholder="What territories are your interested in?"></textarea>
						</div>
						<div class="form_field">
							<input type="text" name="timeframe" id="timeframe" placeholder="What is your timeframe to purchase?">
						</div>
						<div class="form_field">
							<input type="text" name="invest" id="invest" placeholder="How much do you have to invest?">
						</div>
						<div class="form_field">
							<strong>What is the best time to call you:</strong> <br />
							<input type="radio" name="time" value="Morning"> Morning
							<input type="radio" name="time" value="Afternoon"> Afternoon
							<input type="radio" name="time" value="Evenings"> Evenings
						</div>
					</div>
				</form>

				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			</div>
		<?php endif; ?>

		
		
	</div>
<?php
}