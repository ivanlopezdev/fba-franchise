<?php
// this file contains the contents of the popup window
$wpc_url = wpc_url();
function wpc_url() {
	$s        = empty( $_SERVER["HTTPS"] ) ? '' : ( $_SERVER["HTTPS"] == "on" ) ? "s" : "";
	$protocol = substr( strtolower( $_SERVER["SERVER_PROTOCOL"] ), 0, strpos( strtolower( $_SERVER["SERVER_PROTOCOL"] ), "/" ) ) . $s;
	$port     = ( $_SERVER["SERVER_PORT"] == "80" ) ? "" : ( ":" . $_SERVER["SERVER_PORT"] );
	$url      = $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
	$url      = explode( "wp-content", $url );

	return $url[0];
}

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
print_r($parse_uri[0] . 'wp-load.php');
require_once( $parse_uri[0] . 'wp-load.php' );

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Featured Franchises</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo includes_url( 'js/tinymce/tiny_mce_popup.js' ); ?>"></script>


	<script>

		var FranchiseDialog = {
			local_ed: 'ed',
			init    : function (ed) {
				FranchiseDialog.local_ed = ed;
			},

			insert: function insertButton(ed) {

				// Try and remove existing style / blockquote
				tinyMCEPopup.execCommand('mceRemoveNode', false, null);

				var display = jQuery('#display').val();
				var category = jQuery('#category').val();
				var ids = jQuery('#franchises').val();
				var featured = jQuery('#featured');
				var featuredCount = jQuery('#featured_count').val();
				var output = '';


				if (featured.is(':checked')) {
					featured = 'featured=true ';
				}
				else {
					featured = "";
				}

				if (featuredCount != null) {
					featuredCount = 'count=' + featuredCount + '';
				}
				else {
					featuredCount = "count=5 ";
				}

				if (ids != null) {
					ids = 'ids="' + ids + '" ';
				}
				else {
					ids = "";
				}

				if (category != "") {
					category = 'category="' + category + '" ';
				}
				else {
					category = "";
				}

				// setup the output of our shortcode
				output = '[franchises ' + ids + category + 'display="' + display + '" ' + featured + '" ' + featuredCount + ']';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);

				// Return
				tinyMCEPopup.close();
			}
		};
		tinyMCEPopup.onInit.add(FranchiseDialog.init, FranchiseDialog);

	</script>

	<style>
		#display, #category, #franchises {
			width:   300px;
			padding: 6px;
			height:  25px;

		}

		.button-primary {
			padding: 9px;
		}
	</style>


</head>
<body>

<div style="padding:20px;" id="wpc-button-dialog">

	<div style="margin-bottom:15px;">
		<label for="wpc-url" style="margin-right: 60px;">Display</label><br />
		<select class="widefat" name="display" id="display">
			<option value="name">Name</option>
			<option value="logo">Logo</option>
		</select>
	</div>


	<div style="margin-bottom:15px;">
		<label for="wpc-button" style="margin-right: 10px;">Category</label><br />
		<select class="widefat" name="category" id="category">
			<option value="">Select One</option>
			<?php
			$categories = FBA_API::get_categories();
			foreach ( $categories as $cat ): ?>
						<option value="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></option>
			<?php endforeach ?>
		</select>
	</div>

	<div style="margin-bottom:15px;">
		<label for="wpc-featured" style="margin-right: 60px;">Featured</label><br />
		<input type="checkbox" id="featured"> Only Show featured Franchises
	</div>

	<div style="margin-bottom:15px;">
		<label for="wpc-featured" style="margin-right: 60px;">Featured Count</label><br />
		<input type="text" id="featured_count">
	</div>

	<div style="margin-bottom:15px;">
		<label for="wpc-button" style="margin-right: 10px;">Franchises</label><br />
		<select class="widefat" name="franchises[]" id="franchises" multiple style="height:100px">
			<?php
			$franchises = FBA_API::get_franchises( '&minimal=1' );
			foreach ( $franchises as $franchise ): ?>
				<option value="<?php echo $franchise->id; ?>"><?php echo $franchise->name; ?></option>
			<?php endforeach ?>
		</select>
	</div>

	<div>
		<input type="submit" onclick="javascript:FranchiseDialog.insert(FranchiseDialog.local_ed)" class="button-primary" value="Insert Franchises" />
	</div>
</div>
</body>
</html>