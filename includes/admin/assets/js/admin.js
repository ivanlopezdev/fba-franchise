(function ($) {
	"use strict";

	$(function () {

		tinymce.create('tinymce.plugins.fbaFranchises', {
			init: function (ed, url) {
				ed.addButton('fba_franchises', {
					title: 'Featured Franchises',
					cmd  : 'franchises',
				});

				ed.addButton('fba_achivements', {
					title: 'Your Achievements',
					cmd  : 'achievements',
				});

				ed.addCommand('achievements', function () {
					var selected_text = ed.selection.getContent();
					var return_text = '';
					return_text = '[achievements]';
					ed.execCommand('mceInsertContent', 0, return_text);
				});

				url = url.replace('/assets/js', '');

				ed.addCommand('franchises', function () {
					ed.windowManager.open({
						file  : url + '/shortcode-popup.php', // file that contains HTML for our modal window
						width : 450,
						title : 'Featured Franchises',
						height: 380,
						inline: 1
					}, {
						plugin_url: url, // Plugin absolute URL
					});
				});


			},

			// ... Hidden code
		});
		// Register plugin
		tinymce.PluginManager.add('fbaFranchises', tinymce.plugins.fbaFranchises);

	});

}(jQuery));