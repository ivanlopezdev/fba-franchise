<?php

class FBA_Franchise_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by loading admin scripts & styles and adding a
	 * settings page and menu.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		$plugin            = FBA_Franchise::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ), 5 );

		add_filter( "mce_external_plugins", array( $this, "add_buttons" ) );
		add_filter( 'mce_buttons', array( $this, 'register_buttons' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'admin_css' ) );

		add_action( 'cmb2_admin_init', array( $this, 'setting_fields' ) );
		add_action( 'cmb2_admin_init', array( $this, 'exit_setting_fields' ) );
		add_action( 'cmb2_admin_init', array( $this, 'cookie_setting_fields' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 * @since     1.0.0
	 *
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	public function flush_urls() {
		flush_rewrite_rules();
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_screen_hook_suffix = add_menu_page(
			__( 'Franchise Broker Association Settings', $this->plugin_slug ),
			__( 'Franchise BA', $this->plugin_slug ),
			'manage_options',
			'fba_setting',
			array( $this, 'display_plugin_admin_page' ),
			$this->get_icon(),
			82
		);
	}

	public function get_icon() {
		$icon = base64_encode( '<svg width="134" height="134" xmlns="http://www.w3.org/2000/svg" fill="none">
	<path d="M85.5 16.4c0-.5 0-.9-.5-1.1s-.9.1-1.2.5c-.2.3-.3.5-.5.8-6 10.4-12 20.9-18 31.3-.9 1.5-1.4 1.5-2.4.1-4-5.7-7.9-11.3-11.9-17l-.6-.9c-.3-.4-.6-.6-1.1-.4-.5.2-.6.7-.5 1.2.1.3.2.6.2.9l5.4 20.1c.1.3.2.5.2.8.1.7-.3 1.1-1 1.1-.4 0-.8-.1-1.3-.2-4.6-.8-9.2-1.6-13.8-2.5-7.2-1.3-14.4-2.6-21.7-3.8-.2 0-.4-.1-.6-.1-.5-.1-.9.1-1.1.6s.1.8.5 1.1c.3.2.7.4 1 .6 10.4 6 20.7 11.9 31.1 17.9.3.2.6.3.8.5.5.4.5 1 .1 1.4-.2.2-.5.4-.7.6l-14.1 9.9c-1.2.8-2.4 1.7-3.6 2.5-.4.3-.6.6-.4 1.1s.6.7 1.1.5c.2 0 .4-.1.6-.1l4.5-1.2c5.4-1.5 10.8-2.9 16.2-4.3 1.4-.4 1.8 0 1.6 1.4-.5 3.1-1.1 6.2-1.6 9.3-1.5 8.2-2.9 16.4-4.4 24.6-.2 1-.3 1.9-.5 2.9-.1.5.1.9.5 1 .5.2.8 0 1.1-.4.1-.2.3-.5.4-.7 6-10.5 12.1-21 18.1-31.5.9-1.5 1.4-1.5 2.4-.1 4 5.7 8 11.5 12 17.2.2.3.4.6.6.8.3.3.7.4 1.1.1.2-.2.4-.6.4-.9 0-.3-.1-.6-.2-.9-1.8-6.7-3.6-13.5-5.5-20.2-.5-1.8-.1-2.2 1.8-1.8 3.7.7 7.5 1.3 11.2 2 7.2 1.3 14.4 2.6 21.7 3.9l3.6.6c.4.1.8-.1.9-.5.2-.5 0-.8-.4-1.1-.3-.2-.6-.4-.9-.5-10.4-6-20.8-12-31.2-17.9-1.6-.9-1.4-1.7-.1-2.6 5.8-4 11.5-8 17.2-12 .3-.2.6-.3.7-.6.2-.3.3-.7.2-.9s-.5-.4-.8-.5c-.3 0-.6.1-.9.2-6.8 1.8-13.6 3.6-20.3 5.5-1.8.5-2.1.1-1.8-1.7.6-3.2 1.2-6.5 1.7-9.7 1.3-7.2 2.6-14.4 3.8-21.7.3-1.8.6-3.5.9-5.2zM66.4 0c37.3.2 66.4 30.2 66.3 66.5-.1 36.6-29.6 66.3-66.4 66.3C29.6 132.8 0 103 0 66.4 0 30.3 28.9.2 66.4 0z" class="st2"/>
	<path d="M71.9 66.4c0 3.1-2.4 5.5-5.5 5.6-2.5 0-5.6-1.8-5.6-5.6 0-3 2.6-5.6 5.6-5.6s5.5 2.5 5.5 5.6z" class="st8"/>
</svg>' );

		return 'data:image/svg+xml;base64,' . $icon;
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once( 'settings-page.php' );
	}

	/**
	 * Add buttons to editor
	 *
	 * @return array
	 * @since 1.0
	 *
	 */
	public function add_buttons( $plugin_array ) {
		$plugin_array['fbaFranchises'] = FBA_API_PLUGIN_URL . '/includes/admin/assets/js/admin.js';

		return $plugin_array;
	}

	/**
	 * Register buttons to editor
	 *
	 * @return array
	 * @since 1.0
	 *
	 */
	public function register_buttons( $buttons ) {
		array_push( $buttons, 'fba_achivements', 'fba_franchises' );

		return $buttons;
	}

	/**
	 * load admin css
	 *
	 * @return voice
	 * @since 1.0
	 *
	 */
	public function admin_css() {
		wp_enqueue_style( 'fba_admin', FBA_API_PLUGIN_URL . 'includes/admin/assets/css/admin.css', array(), FBA_Franchise::VERSION, 'all' );
	}

	public function setting_fields() {
		$cmb = new_cmb2_box( array(
			'id'           => 'fba_setting',
			'title'        => 'Settings',
			'object_types' => array( 'options-page' ),
			'option_key'   => 'fba_setting',
			'menu_title'   => 'Settings',
			'parent_slug'  => 'fba_setting',
			'capability'   => 'manage_options',
		) );

		$cmb->add_field( array(
			'name' => 'API Key',
			'desc' => 'Enter API key and token to activate the Franchise Broker Association Franchise Search plugin.',
			'type' => 'title',
			'id'   => 'api-key-description'
		) );

		$cmb->add_field( array(
			'name' => 'API Key',
			'id'   => 'api_key',
			'type' => 'text',
		) );

		$cmb->add_field( array(
			'name' => 'API Token',
			'id'   => 'api_token',
			'type' => 'text',
		) );

		$cmb->add_field( array(
			'name' => 'Disable Styles',
			'desc' => 'By selecting this none of the default styles will be loaded.',
			'id'   => 'styles',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'FBA Candidates',
			'desc' => 'Would you like users to have to have to fill out a form to access franchise details.',
			'type' => 'title',
			'id'   => 'fba-candidate'
		) );

		$cmb->add_field( array(
			'name' => 'FBA Form',
			'desc' => 'Activate Franchise Profile Form',
			'id'   => 'api_form',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Questionnaire Result Count',
			'id'   => 'result_count',
			'type' => 'text_small'
		) );

		$cmb->add_field( array(
			'name' => 'Send Pre-registration',
			'desc' => 'Send Pre-registration email to concept when questionnaire is submitted',
			'id'   => 'fba_api_send_pre_registration',
			'type' => 'checkbox',
		) );

		if ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'gravityforms/gravityforms.php' ) && class_exists( 'GFAPI' ) ) {
			$cmb->add_field( array(
				'name' => 'Gravity Forms',
				'desc' => 'Replace plugin forms with gravity forms.',
				'type' => 'title',
				'id'   => 'fba-gravity-forms'
			) );

			$forms = \GFAPI::get_forms( true, false, 'title', 'ASC');
			$form_options = [];

			foreach( $forms as $form ) {
				$form_options[ $form['id'] ] = $form['title'];
			}

			$cmb->add_field( array(
				'name' => 'Request more information form',
				'id'   => 'request_information',
				'type' => 'select',
				'show_option_none' => true,
				'options' => $form_options,
				'description' => 'Replace the form users are presented in order to access additional information about a franchise.'
			) );

			$cmb->add_field( array(
				'name' => 'Territory Check form',
				'id'   => 'territory_check',
				'type' => 'select',
				'show_option_none' => true,
				'options' => $form_options,
				'description' => 'Replace the territory check form.'
			) );
		}
	}

	public function exit_setting_fields() {
		$cmb = new_cmb2_box( array(
			'id'           => 'fba_settings_exit',
			'title'        => 'Exit Intent',
			'object_types' => array( 'options-page' ),
			'option_key'   => 'fba_settings_exit',
			'menu_title'   => 'Exit Intent',
			'parent_slug'  => 'fba_setting',
			'capability'   => 'manage_options',
		) );

		$cmb->add_field( array(
			'name' => 'Exit Intent Settings',
			'desc' => 'Display a popup when a site visitor attempts to leave the site.',
			'type' => 'title',
			'id'   => 'exit-description'
		) );

		$cmb->add_field( array(
			'name' => 'Enable',
			'desc' => 'Enable exit intent modal.',
			'id'   => 'enable',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Show On Delay',
			'desc' => 'If checked, the popup will show after the delay option time. If not checked, popup will show when a visitor moves their cursor above the document window, showing exit intent.',
			'id'   => 'show_on_delay',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Delay',
			'desc' => 'The time, in seconds, until the popup activates and begins watching for exit intent. If "Show On Delay" is checked, this will be the time until the popup shows.',
			'id'   => 'delay',
			'type' => 'text_small',
		) );

		$cmb->add_field( array(
			'name' => 'Width',
			'desc' => 'Set a custom width for the exit intent modal.',
			'id'   => 'width',
			'type' => 'text_small',
		) );

		$cmb->add_field( array(
			'name' => 'Height',
			'desc' => 'Set a custom height for the exit intent modal.',
			'id'   => 'height',
			'type' => 'text_small',
		) );

		$cmb->add_field( array(
			'name' => 'Cookie Expires',
			'desc' => 'The number of days to set the cookie for. A cookie is used to track if the popup has already been shown to a specific visitor. If the popup has been shown, it will not show again until the cookie expires. A value of 0 will always show the popup.',
			'id'   => 'cookie',
			'type' => 'text_small',
		) );

		$cmb->add_field( array(
			'name'    => 'Popup Content',
			'id'      => 'content',
			'type'    => 'wysiwyg',
			'options' => array(),
		) );

		$cmb->add_field( array(
			'name'       => 'Custom CSS',
			'id'         => 'css',
			'type'       => 'textarea_code',
			'attributes' => array(
				'data-codeeditor' => json_encode( array(
					'codemirror' => array(
						'mode' => 'css',
						'type' => 'text/css'
					),
				) ),
			),
			'desc'       => 'Available selectors are #bio_ep_bg, bio_ep and #bio_ep_close',
		) );
	}

	public function cookie_setting_fields() {
		$cmb = new_cmb2_box( array(
			'id'           => 'fba_settings_cookie',
			'title'        => 'Cookie Consent',
			'object_types' => array( 'options-page' ),
			'option_key'   => 'fba_settings_cookie',
			'menu_title'   => 'Cookie Consent',
			'parent_slug'  => 'fba_setting',
			'capability'   => 'manage_options',
		) );

		$cmb->add_field( array(
			'name' => 'Cookie Consent',
			'desc' => 'Enable and configure cookie consent alert.',
			'type' => 'title',
			'id'   => 'cookie-description'
		) );

		$cmb->add_field( array(
			'name'    => 'Placement',
			'id'      => 'placement',
			'type'    => 'select',
			'options' => array(
				''             => 'None',
				'bottom'       => 'Bottom',
				'top'          => 'Top',
				'bottom-left'  => 'Left',
				'bottom-right' => 'Right',
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Theme',
			'id'      => 'theme',
			'type'    => 'select',
			'options' => array(
				'block'    => 'Block',
				'edgeless' => 'Edgeless',
				'classic'  => 'Classic',
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Cookie Message',
			'id'      => 'message',
			'type'    => 'text',
			'default' => 'This website uses cookies to improve your experience.',
		) );

		$cmb->add_field( array(
			'name'    => 'Cookie Dismiss Button Text',
			'id'      => 'button_text',
			'type'    => 'text',
			'default' => 'Ok',
		) );

		$cmb->add_field( array(
			'name'    => 'Cookie Learn More Link Text',
			'id'      => 'link_text',
			'type'    => 'text',
			'default' => 'Learn more',
		) );


		$cmb->add_field( array(
			'name' => 'Cookie Policy URL',
			'id'   => 'policy',
			'type' => 'text_url',
		) );

		$cmb->add_field( array(
			'name'    => 'Popup Background Color',
			'id'      => 'popup_background_color',
			'type'    => 'colorpicker',
			'default' => '#000000',
		) );

		$cmb->add_field( array(
			'name'    => 'Popup Text Color',
			'id'      => 'popup_text_color',
			'type'    => 'colorpicker',
			'default' => '#ffffff',
		) );

		$cmb->add_field( array(
			'name'    => 'Popup Link Color',
			'id'      => 'popup_link_color',
			'type'    => 'colorpicker',
			'default' => '#ffffff',
		) );

		$cmb->add_field( array(
			'name'    => 'Button Background Color',
			'id'      => 'button_background_color',
			'type'    => 'colorpicker',
			'default' => '#f1d600',
		) );

		$cmb->add_field( array(
			'name'    => 'Button Border Color',
			'id'      => 'button_border_color',
			'type'    => 'colorpicker',
			'default' => '#f1d600',
		) );

		$cmb->add_field( array(
			'name'    => 'Button Text Color',
			'id'      => 'button_text_color',
			'type'    => 'colorpicker',
			'default' => '#000000',
		) );

	}
}
