<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   FBA Franchises
 * @author    Ivan Lopez <ivan@wpconsole.com>
 * @license   GPL-2.0+
 * @link      http://wpconsole.com
 * @copyright 2013 WP Console LLC
 */

?>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<form method="POST" action="options.php">
		<?php
		settings_fields( 'fba-franchises' );    //pass slug name of page, also referred

		do_settings_sections( 'fba-franchises' );    //pass slug name of page
		submit_button();
		?>
	</form>

</div>
