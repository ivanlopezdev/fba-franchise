<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package   FBA Franchises
 * @author    Ivan Lopez <ivan@wpconsole.com>
 * @license   GPL-2.0+
 * @link      http://wpconsole.com
 * @copyright 2013 WP Console LLC
 */
class FBA_Franchise {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '2.9.5';

	/**
	 * Unique identifier for your plugin.
	 *
	 *
	 * The variable name is used as the text domain when internationalizing strings
	 * of text. Its value should match the Text Domain file header in the main
	 * plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'fba-franchises';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'init_plugin' ) );

		// Activate plugin when new blog is added
		add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );


		add_action( 'template_redirect', array( $this, 'franchise_route' ) );
		add_filter( 'query_vars', array( $this, 'search_parameters' ) );

		add_action( 'wp', array( $this, 'replace_genesis_sidebar' ) );
		add_action( 'wp_footer', array( $this, 'footer_scripts' ) );

	}

	/**
	 * Return the plugin slug.
	 *
	 * @return    Plugin slug variable.
	 * @since    1.0.0
	 *
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 * @since     1.0.0
	 *
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @param boolean $network_wide          True if WPMU superadmin uses
	 *                                       "Network Activate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       activated on an individual blog.
	 *
	 * @since    1.0.0
	 *
	 */
	public static function activate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_activate();
				}

				restore_current_blog();

			} else {
				self::single_activate();
			}

		} else {
			self::single_activate();
		}

	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @param boolean $network_wide          True if WPMU superadmin uses
	 *                                       "Network Deactivate" action, false if
	 *                                       WPMU is disabled or plugin is
	 *                                       deactivated on an individual blog.
	 *
	 * @since    1.0.0
	 *
	 */
	public static function deactivate( $network_wide ) {

		if ( function_exists( 'is_multisite' ) && is_multisite() ) {

			if ( $network_wide ) {

				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {

					switch_to_blog( $blog_id );
					self::single_deactivate();

				}

				restore_current_blog();

			} else {
				self::single_deactivate();
			}

		} else {
			self::single_deactivate();
		}

	}

	/**
	 * Fired when a new site is activated with a WPMU environment.
	 *
	 * @param int $blog_id ID of the new blog.
	 *
	 * @since    1.0.0
	 *
	 */
	public function activate_new_site( $blog_id ) {

		if ( 1 !== did_action( 'wpmu_new_blog' ) ) {
			return;
		}

		switch_to_blog( $blog_id );
		self::single_activate();
		restore_current_blog();

	}

	/**
	 * Get all blog ids of blogs in the current network that are:
	 * - not archived
	 * - not spam
	 * - not deleted
	 *
	 * @return   array|false    The blog ids, false if no matches.
	 * @since    1.0.0
	 *
	 */
	private static function get_blog_ids() {

		global $wpdb;

		// get an array of blog ids
		$sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";

		return $wpdb->get_col( $sql );

	}

	/**
	 * Fired for each blog when the plugin is activated.
	 *
	 * @since    1.0.0
	 */
	private static function single_activate() {
		update_option( 'fba_franchise_version', self::VERSION );
		flush_rewrite_rules();
	}

	/**
	 * Fired for each blog when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	private static function single_deactivate() {
		delete_option( 'fba_franchise_version', self::VERSION );
		flush_rewrite_rules();
	}

	/**
	 * Registers search parameters
	 *
	 * @since    1.0.0
	 */
	public function search_parameters( $vars ) {
		$vars[] = 'category';   //Token for authentication
		$vars[] = 'MinInv';     //Key for authentication
		$vars[] = 'Liquid';     //Route to brokers
		$vars[] = 'HomeBased';  //Parameter for brokers to see what awards they have
		$vars[] = 'PT';         //Route to franchise category
		$vars[] = 'Staff';      //Route to franchise all or by id
		$vars[] = 'RecessRest'; //Search parameter for min investment
		$vars[] = 'fbasearch';  //Search parameter for min liquidity
		$vars[] = 'state';      //Search parameter for min liquidity
		$vars[] = 'fran-order';    //Search parameter for min liquidity

		return $vars;
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function init_plugin() {

		add_rewrite_tag( '%fbafranchise%', '([^/]+)' );
		$post = get_posts( array( 'name' => 'franchise', 'post_type' => 'page' ) );
		if ( $post ) {
			add_rewrite_rule( '^franchise/([^/]*)/?', 'index.php?page_id=' . $post[0]->ID . '&fbafranchise=$matches[1]', 'top' );
		}

		$args = array(
			'name'          => __( 'Franchise Search', 'fba' ),
			'id'            => 'frabchise-search-sidebar',
			'description'   => 'Sidebar for the franchise search page',
			'class'         => '',
			'before_widget' => '<li id="%1" class="widget %2">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>'
		);

		if ( has_action( 'genesis_sidebar' ) ) {
			register_sidebar( $args );
		}

		$version = get_option( 'fba_version' );

		if ( ! $version ) {
			update_option( 'fba_version', '1.0.0' );
			flush_rewrite_rules();
		}

	}

	/**
	 * Replace Genises sidebar
	 *
	 * @since    1.0.0
	 */
	public function replace_genesis_sidebar() {
		if ( is_page( 'franchise-search' ) ) {

			if ( has_action( 'genesis_sidebar' ) ) {
				remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
				add_action( 'genesis_sidebar', array( $this, 'fba_search_sidebar' ) );
			}
		}
	}

	/**
	 * Generate Franchise search widget
	 *
	 * @since    1.0.0
	 */
	public function fba_search_sidebar() {
		genesis_widget_area( 'frabchise-search-sidebar' );
	}

	/**
	 * Template Redirect for franchise search
	 *
	 * @since    1.0.0
	 */
	public function franchise_route() {
		global $wp_query, $post, $franchise, $fba_settings, $fba_api_form, $fba_check_cookie, $fba_show_details, $fba_show_message;;

		if ( isset( $wp_query->query_vars['fbafranchise'] ) && $slug = $wp_query->query_vars['fbafranchise'] ) {
			$franchise = FBA_API::get_franchise_by_slug( '&slug=' . $slug );

			if ( ! $franchise ) {
				include( get_stylesheet_directory() . '/404.php' );
			} else {
				add_filter( 'wp_title', array( $this, 'franchise_page_title' ) );
				add_filter( 'the_title', array( $this, 'franchise_title' ), 10, 2 );
				add_filter( 'the_content', array( $this, 'load_details_page' ) );
				add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

				$franchise = $franchise[0];

				$fba_settings     = (array) get_option( 'fba_setting' );
				$fba_api_form     = isset( $fba_settings['api_form'] ) ? esc_attr( $fba_settings['api_form'] ) : 0;
				$fba_check_cookie = ( $fba_api_form ) ? true : false;
				$fba_show_details = true;
				$fba_show_message = false;

				if ( $fba_check_cookie ) {
					if ( ! isset( $_COOKIE[ 'franchise_' . $franchise->id ] ) ) {
						$fba_show_details = false;
					}
				}
			}
		}
	}

	/**
	 * Title tag for franchise details
	 *
	 * @since    1.0.0
	 */
	public function franchise_page_title( $title ) {
		return get_bloginfo( 'name' ) . ' - ' . fba_franchise_title();
	}

	/**
	 * Title tag for franchise details
	 *
	 * @since    1.0.0
	 */
	public function franchise_title( $title, $id = null ) {
		global $franchise;
		if ( isset( $franchise ) && isset( $franchise->logo ) && false !== strpos( 'franchise', $title ) ) {
			return fba_franchise_title();
		}

		return $title;
	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-grid', FBA_API_PLUGIN_URL . 'assets/css/grid.css', array(), strtotime( 'NOW' ) );
		wp_enqueue_style( $this->plugin_slug . '-fancybox', FBA_API_PLUGIN_URL . 'assets/js/fancybox/jquery.fancybox.css', array(), self::VERSION );
		wp_enqueue_style( $this->plugin_slug . '-select2', FBA_API_PLUGIN_URL . 'assets/css/select2.min.css', array(), self::VERSION );

		$settings = (array) get_option( 'fba_setting' );
		$styles   = isset( $settings['styles'] ) ? esc_attr( $settings['styles'] ) : 0;
		if ( 0 === $styles ) {
			wp_enqueue_style( $this->plugin_slug . '-plugin-styles', FBA_API_PLUGIN_URL . 'assets/css/styles.css', array(), strtotime( 'NOW' ) );

		}
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-validation', FBA_API_PLUGIN_URL . 'assets/js/validate.min.js', array( 'jquery' ), self::VERSION );
		wp_enqueue_script( $this->plugin_slug . '-fancybox-v2', FBA_API_PLUGIN_URL . 'assets/js/fancybox/jquery.fancybox.js', array( 'jquery' ), self::VERSION );
		wp_enqueue_script( $this->plugin_slug . '-select2', FBA_API_PLUGIN_URL . 'assets/js/select2.min.js', array( 'jquery' ), self::VERSION );
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', FBA_API_PLUGIN_URL . 'assets/js/public.js', array( 'jquery' ), self::VERSION, true );

		$settings = (array) get_option( 'fba_setting' );
		$count    = isset( $settings['result_count'] ) ? esc_attr( $settings['result_count'] ) : 10;

		wp_localize_script( $this->plugin_slug . '-plugin-script', 'fba',
			array(
				'ajax_url'   => admin_url( 'admin-ajax.php' ),
				'nonce'      => wp_create_nonce( "fba-form" ),
				'max_count'  => $count,
				'base_url'   => home_url( '/franchise/' ),
				'plugin_url' => FBA_API_PLUGIN_URL
			) );

		$exit_intent = get_option( 'fba_settings_exit', array() );
		if ( ! empty( $exit_intent['enable'] ) ) {
			wp_enqueue_script( $this->plugin_slug . '-plugin-bioep', FBA_API_PLUGIN_URL . 'assets/js/bioep.min.js', array( 'jquery' ), self::VERSION );
		}

		$cookie_consent = get_option( 'fba_settings_cookie', array() );
		if ( ! empty( $cookie_consent['placement'] ) ) {
			wp_enqueue_script( $this->plugin_slug . '-plugin-cookie-consent', FBA_API_PLUGIN_URL . 'assets/js/cookieconsent.min.js', array( 'jquery' ), self::VERSION );
		}

	}

	public function load_details_page( $content ) {
		return fba_franchise_details_template( $content );
	}

	public function load_details_title( $value ) {
		return fba_franchise_title();
	}

	public function footer_scripts() {
		$exit_intent    = get_option( 'fba_settings_exit', array() );
		$cookie_consent = get_option( 'fba_settings_cookie', array() );

		if ( ! empty( $exit_intent['enable'] ) && ! empty( $exit_intent['content'] ) ) {
			?>
			<script>
				bioEp.init( {
					width             : <?php echo empty( $exit_intent['width'] ) ? 394 : (int) $exit_intent['width'] ?>,
					height            :  <?php echo empty( $exit_intent['height'] ) ? 298 : (int) $exit_intent['height'] ?>,
					cookieExp         : <?php echo ! isset( $exit_intent['cookie'] ) ? 1 : (int) $exit_intent['cookie']  ?>,
					delay             : <?php echo ! isset( $exit_intent['delay'] ) ? 5 : (int) $exit_intent['delay']  ?>,
					showOnDelay       : <?php echo ! isset( $exit_intent['show_on_delay'] ) ? 'false' : 'true'  ?>,
					showOncePerSession: true,
				} );
			</script>

			<style>

				#bio_ep_close {
					left:        unset;
					margin:      unset;
					right:       -13px;
					top:         -11px;
					font-size:   18px;
					line-height: 23px;
				}

				<?php echo empty( $exit_intent['css'] ) ? '' : $exit_intent['css'] ?>
			</style>
			<div id="bio_ep">
				<div id="bio_ep_close">&times;</div>
				<?php echo apply_filters( 'the_content', wp_kses_post( $exit_intent['content'] ) ) ?>
			</div>
			<?php
		}

		if ( ! empty( $cookie_consent['placement'] ) ) {
			?>
			<div id="cookieconsent"></div>
			<script>
				window.cookieconsent.initialise( {
					container : document.getElementById( "cookieconsent" ),
					palette   : {
						popup : {
							background: '<?php echo isset( $cookie_consent['popup_background_color'] ) ? $cookie_consent['popup_background_color'] : '#000000'; ?>',
							text      : '<?php echo isset( $cookie_consent['popup_text_color'] ) ? $cookie_consent['popup_text_color'] : '#ffffff'; ?>',
							link      : '<?php echo isset( $cookie_consent['popup_link_color'] ) ? $cookie_consent['popup_link_color'] : '#ffffff'; ?>',
						},
						button: {
							background: '<?php echo isset( $cookie_consent['button_background_color'] ) ? $cookie_consent['button_background_color'] : '#f1d600'; ?>',
							border    : '<?php echo isset( $cookie_consent['button_border_color'] ) ? $cookie_consent['button_border_color'] : '#f1d600'; ?>',
							text      : '<?php echo isset( $cookie_consent['button_text_color'] ) ? $cookie_consent['button_text_color'] : '#000000'; ?>'
						},
					},
					"position": '<?php echo isset( $cookie_consent['placement'] ) ? $cookie_consent['placement'] : 'bottom-left'; ?>',
					"theme"   : '<?php echo isset( $cookie_consent['theme'] ) ? $cookie_consent['theme'] : 'block'; ?>',
					"domain"  : "<?php echo home_url(); ?>",
					"secure"  : true,
					"content" : {
						"message": "<?php echo $cookie_consent['message']; ?>",
						"dismiss": "<?php echo $cookie_consent['button_text']; ?>",
						"link"   : "<?php echo $cookie_consent['link_text'] ?? ''; ?>",
						"href"   : "<?php echo $cookie_consent['policy'] ?? ''; ?>",
						"close"  : '&#x274c;',
						"policy" : 'Cookie Policy',
						"target" : '_blank',
					}
				} )
			</script>
			<?php
		}

	}

}
