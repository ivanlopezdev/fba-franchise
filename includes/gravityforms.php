<?php

GFForms::include_addon_framework();

class FBAAddOn extends GFAddOn {

	protected $_version = '1.0';
	protected $_min_gravityforms_version = '1.9';
	protected $_slug = 'fbamembers';
	protected $_path = 'includes/gravityforms.php';
	protected $_full_path = __FILE__;
	protected $_title = 'FBA Members';
	protected $_short_title = 'FBA Members';

	private static $_instance = null;


	/**
	 * Get an instance of this class.
	 *
	 * @return FBAAddOn
	 */
	public static function get_instance() {
		if ( self::$_instance == null ) {
			self::$_instance = new FBAAddOn();
		}

		return self::$_instance;
	}

	/**
	 * Handles hooks and loading of language files.
	 */
	public function init() {
		parent::init();
		add_action( 'gform_field_standard_settings', array( $this, 'mapping_settings' ), 10, 2 );
		add_filter( 'gform_tooltips', array( $this, 'mapping_tooltips' ) );
		add_action( 'gform_editor_js', array( $this, 'editor_script' ) );
		add_filter( 'gform_confirmation', array( $this, 'custom_confirmation' ), 10, 4 );
		add_filter( 'wp_footer', array( $this, 'add_modals' ), 1 );
	}

	/**
	 * Configures the settings which should be rendered on the Form Settings > Simple Add-On tab.
	 *
	 * @return array
	 */
	public function form_settings_fields( $form ) {
		$choices = [
			array(
				'label' => 'Select one',
				'value' => '',
			)
		];
		$tags    = FBA_API::get_mailchimp_tags();

		foreach ( $tags->tags as $tag ) {
			$choices[] = array(
				'label' => $tag,
				'value' => $tag,
			);
		}

		return array(
			array(
				'title'  => 'FBA Members Settings',
				'fields' => array(
					array(
						'label'   => 'Mailchimp Auto Tag',
						'type'    => 'select',
						'name'    => 'mailchimp_tag',
						'tooltip' => 'Auto assign mailchimp tag when form is submitted',
						'choices' => $choices,
					),
					array(
						'label'   => 'Create FBA Candidate',
						'type'    => 'checkbox',
						'name'    => 'fba_create_candidate',
						'tooltip' => 'Generate candidate within FBAMembers.com',
						'choices' => array(
							array(
								'label' =>  'Enabled',
								'name'  => 'fba_create_candidate',
							),
						),
					),
					array(
						'label'   => 'FBA Recommended Franchises',
						'type'    => 'checkbox',
						'name'    => 'fba_recommended_franchises',
						'tooltip' => 'Return a list of recommended concepts from FBAMembers.com',
						'choices' => array(
							array(
								'label' =>  'Enabled',
								'name'  => 'fba_recommended_franchises',
							),
						),
					),
					array(
						'label'   => 'Create Hubspot BOS Contact',
						'type'    => 'checkbox',
						'name'    => 'fba_hubspot',
						'tooltip' => 'Create a contact within Hubspot BOS',
						'choices' => array(
							array(
								'label' =>  'Enabled',
								'name'  => 'fba_hubspot',
							),
						),
					),
				),
			),
		);
	}

	public function mapping_settings( $position, $form_id ) {

		if ( $position === 25 ) {
			$settings = $this->get_form_settings( GFAPI::get_form( $form_id ) );

			if ( ! empty( $settings['fba_create_candidate'] ) || ! empty( $settings['fba_hubspot'] ) ) : ?>

				<li class="fba_member_setting field_setting">
					<label for="field_admin_label" class="section_label">
						FBA Member Mapping Field
						<?php gform_tooltip( 'form_field_fba_mapping_value' ) ?>
					</label>
					<select id="fba-mapping-field" onchange="SetFieldProperty('FBAMappingField', this.value);">
						<option value="">Select One</option>
						<option value="_fba_first_name">First Name (BOS & FBA)</option>
						<option value="_fba_last_name">Last Name (BOS & FBA)</option>
						<option value="_fba_phone">Phone Number (BOS & FBA)</option>
						<option value="_fba_email">Email (BOS & FBA)</option>
						<option value="address">Address (BOS)</option>
						<option value="_fba_city">City (BOS & FBA)</option>
						<option value="_fba_state">State (BOS & FBA)</option>
						<option value="_fba_zip">Zipcode (BOS & FBA)</option>
						<option value="_fba_invest">Max Investment (FBA)</option>
				 		<option value="_fba_fba_networth">Net Worth (FBA)</option>
						<option value="_fba_liquid_cash">Liquid Cash (BOS & FBA)</option>
						<option value="geo">Geographical Location (BOS)</option>
						<option value="note">Note (BOS)</option>
						<option value="franchise">Franchise (BOS)</option>
						<option value="first-page">First Page Seen (BOS)</option>
					</select>
				</li>

			<?php endif;
		}

	}

	public function mapping_tooltips( $tooltips ) {
		$tooltips['form_field_fba_mapping_value'] = "<h6>Mapping</h6> Select what field within the FBAMembers.com candiate profile that you would like this field to populate.";

		return $tooltips;
	}

	public function editor_script() {
		?>
		<script type='text/javascript'>
			//adding setting to fields of type "text"
			<?php
			foreach ( GF_Fields::get_all() as $gf_field ) {
				echo 'fieldSettings.' . $gf_field->type . ' += ", .fba_member_setting";' . PHP_EOL;
			}
			?>

			//binding to the load field settings event to initialize the checkbox
			jQuery( document ).on( 'gform_load_field_settings', function ( event, field, form ) {
				jQuery( '#fba-mapping-field' ).val( field.FBAMappingField );
			} );
		</script>
		<?php
	}

	public function custom_confirmation( $confirmation, $form, $entry, $ajax ) {
		$settings = $this->get_form_settings( $form );
		if( ! empty( $settings['fba_hubspot'] ) ) {
			$mapping = [
				'lead-source' => 'Website'
			];

			$fields = array(
				'_fba_first_name'   => 'first-name',
				'_fba_last_name'    => 'last-name',
				'address'         	=> 'address',
				'_fba_city'         => 'city',
				'_fba_state'        => 'state',
				'_fba_zip'          => 'zip',
				'_fba_phone'        => 'phone',
				'_fba_email'        => 'email',
				'_fba_liquid_cash'  => 'liquid-capital',
				'franchise'  		=> 'franchise',
				'notes'  			=> 'note',
				'geo' 				=> 'geographical-location',
				'first-page' 		=> 'first-page',
			);

			foreach ( $form['fields'] as $field_key => $field ) {
				if ( isset( $field->FBAMappingField ) && ! empty( $field->FBAMappingField ) ) {

					if ( isset( $entry[ $field->id ] ) && ! empty( $entry[ $field->id ] ) ) {
						$mapping[ $fields[ $field->FBAMappingField ] ] = $entry[ $field->id ];
					}
				}
			}

			if ( ! empty( $mapping['email'] ) && ! empty( $mapping['first-name'] ) && ! empty( $mapping['last-name'] ) ) {
				$bos_endpoint = FBA_API::submit_bos( $mapping );
			}
		}

		if( ! empty(  $settings['fba_recommended_franchises'] )  && ! empty( $settings['fba_create_candidate'] ) ) {
			$mapping = array(
				'_fba_first_name'   => '',
				'_fba_last_name'    => '',
				'_fba_city'         => '',
				'_fba_state'        => '',
				'_fba_zip'          => '',
				'_fba_phone'        => '',
				'_fba_email'        => '',
				'_fba_invest'       => '',
				'_fba_fba_networth' => '',
				'_fba_liquid_cash'  => '',
			);

			foreach ( $form['fields'] as $field_key => $field ) {
				if ( isset( $field->FBAMappingField ) && ! empty( $field->FBAMappingField ) ) {

					if ( isset( $entry[ $field->id ] ) && ! empty( $entry[ $field->id ] ) ) {
						$mapping[ $field->FBAMappingField ] = $entry[ $field->id ];
					}
				}
			}
			if ( ! empty( $mapping['_fba_email'] ) && ! empty( $mapping['_fba_first_name'] ) && ! empty( $mapping['_fba_last_name'] ) ) {
				if ( ! empty( $settings['mailchimp_tag'] ) ) {
					$mapping['tag'] = $settings['mailchimp_tag'];
				}
				$results = FBA_API::submit_questionnaire( $mapping, ! empty( $settings['fba_recommended_franchises'] ) );

				if( count( $results ) > 0 ){
					ob_start();
					echo '<div class="gridRow">
					<div class="grid_12">
						<h2>Below is a small list of franchises that fit based on the info you provided. To get results that are a better match for you schedule a call with us.</h2>
					</div>
				</div>';

					foreach ($results as $result) {
						?>
						<div class="result gridRow">
							<div class="result-image grid_3">
								<?php fba_franchise_listing_logo( $result ) ?>
							</div>
							<div class="result-content grid_6">
								<?php fba_franchise_listing_details( $result ) ?>
							</div>
							<div class="result-image grid_3">
								<?php fba_franchise_data( $result ) ?>
							</div>
						</div>
					<?php
					}

					echo "<script>window.top.loadFancyBox();</script>";

					$confirmation = ob_get_clean();
				} else {
					$confirmation = '<div class="gridRow"><div class="grid_12"><p>We were not able to automatically find concept that match your criteria but will be in contact soon to help you find the right concept for you.</p></div></div>';
				}
			}
			
		} 
		return $confirmation;
	}

	public function add_modals(){
		if( is_singular('page') ){
		?>
			
			<div style="display: none">
			<div id="request-information-modal">
				<h4>Request More Information on <span class="franchise-name"></span></h4>
				<?php $fba_settings = (array) get_option( 'fba_setting' ); ?>
				<?php if( ! empty( $fba_settings['request_information'] ) ): ?>
					<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['request_information'] ?>">
						<?php if(function_exists('gravity_form')){
							gravity_form( $fba_settings['request_information'], false, false, false, '', true );
						} ?>
					</div>
				<?php else: ?>
					<div class="form_message"></div>
					<div class="grid_5">
						<form action="" method="post" name="search_information" id="fba-search-request">
							<div class="form_field">
								<input type="text" name="first_name" placeholder="First Name">
							</div>
							<div class="form_field">
								<input type="text" name="last_name" placeholder="Last Name">
							</div>
							<div class="form_field">
								<input type="text" name="email" placeholder="Email">
							</div>
							<div class="form_field">
								<input type="text" name="phone" placeholder="Phone Number">
							</div>
							<div class="form_field">
								<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
							</div>
							<div class="btn_field">
								<input type="hidden"  name="action_type" value="franchise">
								<input type="submit" name="submit" value="Get More Info!">
							</div>

						</form>
					</div>
				<?php endif; ?>
				<div class="grid_7">
					<?php dynamic_sidebar( 'franchise-search-form' ); ?>
				</div>

				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			</div>
		</div>

		<div style="display: none">
			<div id="territory-check-modal">
				<h4>Territory Check on <span class="franchise-name"></span></h4>
				<?php if( ! empty( $fba_settings['territory_check'] ) ): ?>
					<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['territory_check'] ?>">
						<?php if(function_exists('gravity_form')){
							gravity_form( $fba_settings['territory_check'], false, false, false, '', true );
						} ?>
					</div>
				<?php else: ?>
					<div class="form_message"></div>
					<form action="" method="post" name="territory_information" id="fba-territory-request">

						<div class="grid_5">
							<div class="form_field">
								<input type="text" name="first_name" placeholder="First Name">
							</div>
							<div class="form_field">
								<input type="text" name="last_name" placeholder="Last Name">
							</div>
							<div class="form_field">
								<input type="text" name="email" placeholder="Email">
							</div>
							<div class="form_field">
								<input type="text" name="phone" placeholder="Phone Number">
							</div>
							<div class="form_field">
								<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
							</div>
							<div class="btn_field">
								<input type="hidden" name="action_type" value="territory">
								<input type="submit" name="submit" value="Get More Info!">
							</div>

						</div>

						<div class="grid_7">
							<div class="form_field">
								<textarea name="territories" cols="30" rows="2" placeholder="What territories are your interested in?"></textarea>
							</div>
							<div class="form_field">
								<input type="text" name="timeframe" id="timeframe" placeholder="What is your timeframe to purchase?">
							</div>
							<div class="form_field">
								<input type="text" name="invest" id="invest" placeholder="How much do you have to invest?">
							</div>
							<div class="form_field">
								<strong>What is the best time to call you:</strong> <br />
								<input type="radio" name="time" value="Morning"> Morning
								<input type="radio" name="time" value="Afternoon"> Afternoon
								<input type="radio" name="time" value="Evenings"> Evenings
							</div>
						</div>
					</form>
				<?php endif; ?>

				<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
			</div>
		</div>
		<?php
		}
	}

}
