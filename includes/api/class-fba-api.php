<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * API Call Functions
 */
class FBA_API {

	public static function get_status() {
		return json_decode( self::call_api( 'broker' ) );
	}

	public static function get_achivments() {
		return json_decode( self::call_api( 'broker', '&awards=1' ) );
	}

	public static function get_bos_endpoint() {
		return json_decode( self::call_api( 'broker', '&bos=1' ) );
	}

	public static function submit_bos( $data ){
		$bos_endpoint = self::get_bos_endpoint();

		$results = wp_remote_post( $bos_endpoint->broker , array(
			'body' => $data
		) );

		return json_decode( wp_remote_retrieve_body( $results ), true );
	}

	public static function get_franchises( $args = "" ) {

		return json_decode( json_encode( jsonh_decode( self::call_api( 'franchise', $args ), true ) ) );
	}

	public static function get_featured() {
		return json_decode( json_encode( jsonh_decode( self::call_api( 'franchise', '&featured=1' ), true ) ) );
	}

	public static function get_search_results( $args = "" ) {
		return json_decode( json_encode( jsonh_decode( self::call_api( 'search', $args ), true ) ) );
	}

	public static function get_franchise_by_id( $id, $min = "" ) {
		$args    = ( is_array( $id ) ) ? implode( ",", $id ) : $id;
		$args    = '&id=' . $args;
		$args    .= '&' . $min;
		$results = self::call_api( 'franchise', $args );

		return json_decode( json_encode( jsonh_decode( $results, true ) ) );

	}

	public static function get_franchise_by_slug( $args ) {
		return json_decode( json_encode( jsonh_decode( self::call_api( 'franchise', $args ), true ) ) );
	}

	public static function call_api( $call, $param = "" ) {
		$settings  = (array) get_option( 'fba_setting' );
		$api_key   = esc_attr( $settings['api_key'] );
		$api_token = esc_attr( $settings['api_token'] );

		$cache_key = sanitize_title( $call . ' ' . $param );
		$results = wp_cache_get( $cache_key, 'fba' );

		if( ! empty( $results ) ) {
			return $results;
		}

		$results = wp_remote_get( 'https://fbamembers.com/fba-api/' . $call . '/?key=' . $api_key . '&token=' . $api_token . $param );

		wp_cache_set( $cache_key, wp_remote_retrieve_body( $results ), 'fba', WEEK_IN_SECONDS );

		return wp_remote_retrieve_body( $results );
	}

	public static function submit_questionnaire( $body, $pre_register = true ) {
		$settings                 = (array) get_option( 'fba_setting' );
		$api_key                  = esc_attr( $settings['api_key'] );
		$api_token                = esc_attr( $settings['api_token'] );

		/* if( $pre_register && 1 === (int) $settings['fba_api_send_pre_registration'] ){
			$body['pre_registration'] = 'true';
		} */

		$results = wp_remote_post( 'https://fbamembers.com/fba-api/questionnaire/?key=' . $api_key . '&token=' . $api_token, array(
			'body' => $body
		) );

		return json_decode( json_encode( jsonh_decode( wp_remote_retrieve_body( $results ), true ) ) );
	}

	public static function get_categories() {
		return json_decode( json_encode( jsonh_decode( self::call_api( 'categories' ), true ) ) );
	}

	public static function get_mailchimp_tags() {
		return json_decode( self::call_api( 'tags' ) );
	}

	public static function get_states() {
		$fba_state_list = array(
			''   => "Select a State",
			'AL' => "Alabama",
			'AK' => "Alaska",
			'AZ' => "Arizona",
			'AR' => "Arkansas",
			'CA' => "California",
			'CO' => "Colorado",
			'CT' => "Connecticut",
			'DE' => "Delaware",
			'DC' => "District Of Columbia",
			'FL' => "Florida",
			'GA' => "Georgia",
			'HI' => "Hawaii",
			'ID' => "Idaho",
			'IL' => "Illinois",
			'IN' => "Indiana",
			'IA' => "Iowa",
			'KS' => "Kansas",
			'KY' => "Kentucky",
			'LA' => "Louisiana",
			'ME' => "Maine",
			'MD' => "Maryland",
			'MA' => "Massachusetts",
			'MI' => "Michigan",
			'MN' => "Minnesota",
			'MS' => "Mississippi",
			'MO' => "Missouri",
			'MT' => "Montana",
			'NE' => "Nebraska",
			'NV' => "Nevada",
			'NH' => "New Hampshire",
			'NJ' => "New Jersey",
			'NM' => "New Mexico",
			'NY' => "New York",
			'NC' => "North Carolina",
			'ND' => "North Dakota",
			'OH' => "Ohio",
			'OK' => "Oklahoma",
			'OR' => "Oregon",
			'PA' => "Pennsylvania",
			'RI' => "Rhode Island",
			'SC' => "South Carolina",
			'SD' => "South Dakota",
			'TN' => "Tennessee",
			'TX' => "Texas",
			'UT' => "Utah",
			'VT' => "Vermont",
			'VA' => "Virginia",
			'WA' => "Washington",
			'WV' => "West Virginia",
			'WI' => "Wisconsin",
			'WY' => "Wyoming"
		);

		return $fba_state_list;
	}

}