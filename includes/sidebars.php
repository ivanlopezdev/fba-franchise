<?php
add_action( 'init', 'fba_register_details_sidebar');

function fba_register_details_sidebar(){

	register_sidebar(array(
		'name' => __( 'Franchise Search Form' ),
		'id' => 'franchise-search-form',
		'class'         => 'widget-wrap',
		'description' => __( 'This is the  widget area on the franchise search form' ),
		'before_title' => '<h4>',
		'after_title' => '</h4>',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	));

	register_sidebar(array(
		'name' => __( 'Franchise Details' ),
		'id' => 'franchise-details',
		'class'         => 'widget-wrap',
		'description' => __( 'This is the widget area on the franchise details page' ),
		'before_title' => '<h4>',
		'after_title' => '</h4>',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	));
}