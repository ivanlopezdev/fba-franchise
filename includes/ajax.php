<?php


add_action( 'wp_ajax_information_request', 'fba_send_email' );
add_action( 'wp_ajax_nopriv_information_request', 'fba_send_email' );

function fba_send_email() {
	check_ajax_referer( 'fba-form', 'nonce' );

	add_filter( 'wp_mail_from_name', 'fba_wp_mail_from_name' );

	if ( isset( $_POST['franchise'] ) ) {
		if ( ! empty( $_POST['franchise'] ) ) {
			$franchise = $_POST['franchise'];
			$message   = "";
			$list      = false;
			if ( 'franchise' == $_POST['action_type'] || 'information_request' == $_POST['action_type'] ) {
				$subject = 'Information Request: ' . esc_attr( $franchise['name'] );
				$message .= "Franchise: " . esc_attr( $franchise['name'] ) . "\r\n";
			} elseif ( 'list' == $_POST['action_type'] ) {
				$subject = 'Information Request for ' . count( $franchise ) . ' business concepts';
				$message .= "Franchises: \r\n";
				foreach ( $franchise as $concept ) {
					$message .= $concept['name'] . " \r\n";
				}
				$message .= " \r\n";
				$list    = true;
			} elseif ( 'territory' == $_POST['action_type'] ) {
				$subject = 'Territory Check: ' . esc_attr( $franchise['name'] );
				$message .= "Franchise: " . esc_attr( $franchise['name'] ) . "\r\n";

			} else {
				wp_send_json_error();
			}

			$message .= "Contact Information:\r\n";
			$message .= 'First Name:' . $_POST['first_name'] . "\r\n";
			$message .= 'Last Name:' . $_POST['last_name'] . "\r\n";
			$message .= 'Email:' . $_POST['email'] . "\r\n";
			$message .= 'Phone:' . $_POST['phone'] . "\r\n\r\n";

			if ( isset( $_POST['comment'] ) && ! empty( $_POST['comment'] ) ) {
				$message .= $_POST['comment'];
			}
			if ( isset( $_POST['territories'] ) && ! empty( $_POST['territories'] ) ) {
				$message .= "Territories: ";
				$message .= $_POST['territories'] . "\r\n";
			}
			if ( isset( $_POST['timeframe'] ) && ! empty( $_POST['timeframe'] ) ) {
				$message .= "Timeframe:";
				$message .= $_POST['timeframe'] . "\r\n";
			}
			if ( isset( $_POST['invest'] ) && ! empty( $_POST['invest'] ) ) {
				$message .= "Investment: ";
				$message .= $_POST['invest'] . "\r\n";
			}
			if ( isset( $_POST['time'] ) && ! empty( $_POST['time'] ) ) {
				$message .= "Best time to call: ";
				$message .= $_POST['time'] . "\r\n";
			}

			wp_mail( get_bloginfo( 'admin_email' ), $subject, $message );

			if ( $list ) {
				foreach ( $franchise as $concept ) {
					setcookie( 'franchise_' . $concept['id'], 1, strtotime( '+30 days' ), '/', $_SERVER['HTTP_HOST'] );
				}
			} else {
				setcookie( 'franchise_' . $franchise['id'], 1, strtotime( '+30 days' ), '/', $_SERVER['HTTP_HOST'] );
			}

			wp_send_json_success();
		}
	}

	wp_send_json_error();
}

function fba_wp_mail_from_name( $original_email_from ) {
	return 'Website Lead';
}


add_action( 'wp_ajax_questionnaire', 'fba_send_questionnaire' );
add_action( 'wp_ajax_nopriv_questionnaire', 'fba_send_questionnaire' );

function fba_send_questionnaire() {
	check_ajax_referer( 'fba-form', 'nonce' );

	add_filter( 'wp_mail_from_name', 'fba_wp_mail_from_name' );

	if ( isset( $_POST['_fba_first_name'] ) ) {
		$results = FBA_API::submit_questionnaire( $_POST );
		wp_send_json_success( $results );
	}

	wp_send_json_error();
}

add_action( 'wp_ajax_refresh_gravity_forms', 'refresh_gravity_forms' );
add_action( 'wp_ajax_refresh_gravity_forms', 'refresh_gravity_forms' );

function refresh_gravity_forms() {
	check_ajax_referer( 'fba-form', 'nonce' );

	if ( isset( $_POST['gf_id'] ) ) {
		gravity_form( $_POST['gf_id'], false, false, false, '', true );
		die();
	}

	wp_send_json_error();
}
