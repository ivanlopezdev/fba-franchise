<?php


function fba_search_form( $url = "", $show_list = true, $show_clear = true ) {
	global $wp_query, $franchise_args;
	$args = "&";
	if ( isset( $wp_query->query_vars['category'] ) && $wp_query->query_vars['category'] != "" ) {
		$catID = $wp_query->query_vars['category'];
		$args .= 'cid=' . $catID . '&';
	}
	/*if (isset( $wp_query->query_vars['MinInv'] ) &&  $wp_query->query_vars['MinInv']  != "0"){
		$min =  esc_attr( $wp_query->query_vars['MinInv']  );
		$min = explode('-', $min );
		$args .= 'minInv=' . $min[1] . '&';
	}
	if (isset( $wp_query->query_vars['state'] ) &&  $wp_query->query_vars['state']  != "") {
		$args .= 'state=' . strtolower( esc_attr( $wp_query->query_vars['state']  ) ) ;
	}*/
	if ( isset( $wp_query->query_vars['fran-order'] ) && $wp_query->query_vars['fran-order'] != "" ) {
		$args .= 'fran-order=' . strtolower( esc_attr( $wp_query->query_vars['fran-order'] ) );
	}
	$franchise_args = $args;
	?>
	<div class="fba-primary-search fba-search-bar">
		<div class="<?php echo $show_list ? 'grid_8' : 'grid_12' ?>">
			<form action="<?php echo $url; ?>" method="GET" id="search-form">
				<select name="category" id="category" placeholder="Select a Category" onchange="this.form.submit()">
					<option value="">Select a Category</option>

					<?php
					$categories = FBA_API::get_categories();
					foreach ( $categories as $cat ): 
							if( '' == $cat->name ){
								continue;
							}
						?>
						<option value="<?php echo $cat->slug; ?>" <?php selected($cat->slug, $_GET['category']) ?>><?php echo $cat->name; ?></option>
					<?php endforeach ?>
				</select>

				<select name="fran-order" id="fran-order" placeholder="Sort By" onchange="this.form.submit()">
					<option value="">Order By</option>
					<option value="avghighest" <?php selected( $_GET['fran-order'], 'avghighest' ) ?>>Avg. Investment: Highest to Lowest</option>
					<option value="avglowest" <?php selected( $_GET['fran-order'], 'avglowest' ) ?>>Avg. Investment: Lowest to Highest</option>
					<option value="minhighest" <?php selected( $_GET['fran-order'], 'minhighest' ) ?>>Min. Investment: Highest to Lowest</option>
					<option value="minlowest" <?php selected( $_GET['fran-order'], 'minlowest' ) ?>>Min. Investment: Lowest to Highest</option>
					<option value="master" <?php selected( $_GET['fran-order'], 'master' ) ?>>Area/Master Developer</option>
				</select>
				<?php if ( $show_clear ): ?>
					<input type="button" id="clear" value="Clear">
				<?php endif; ?>
			</form>
		</div>

		<?php if ( $show_list ): ?>
			<div class="grid_4 fba-align-right">
				<a href="#franchise-list-modal" id="viewFranchiseList">View Your Franchise List</a>
			</div>
		<?php endif; ?>

		<div class="clearfix"></div>

		<div class="fba-credits">
			<h2>FranLink System&trade;</h2>
			<span class="tagline">Powered by the Franchise Broker Association</span>
		</div>
	</div>
<?php
}

