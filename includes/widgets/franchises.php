<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */
class FBA_Franchises_Widget extends WP_Widget {

	/**
	 * Constructor
	 *
	 * @return void
	 **/
	public function __construct() {

		parent::__construct(
			'fba-franchises',
			__( 'FBA Franchises', 'fba' ),
			array(
				'classname'   => 'fba-franchises-class',
				'description' => __( 'Display a list of FBA franchises.', 'fba' )
			)
		);
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array  An array of standard parameters for widgets in this theme
	 * @param array  An array of settings for this widget instance
	 *
	 * @return void Echoes it's output
	 **/
	public function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		echo $before_title;
		echo ( ! empty( $title ) ) ? $title : 'Featured Franchises'; // Can set this with a widget option, or omit altogether
		echo $after_title;

		$active = get_transient( 'fba_account_status' );

		if ( ! $active ) {
			$status = FBA_API::get_status();
			$active = $status->broker;
			set_transient( 'fba_account_status', $active, 86400 );
		}

		echo "<div id='featured-franchise-widget'>";

		if ( $active ) {
			$instance = wp_parse_args( (array) $instance );
			extract( $instance );

			$display        = ( isset( $display ) ) ? $display : "name";
			$category       = ( isset( $category ) ) ? $category : "";
			$franchises     = ( isset( $franchises ) ) ? $franchises : "";
			$featured_count = ( isset( $featured_count ) ) ? $featured_count : "";
			$count          = 0;
			if ( isset( $featured ) ) {
				$franchises    = FBA_API::get_featured();
				$featured_type = true;
			} elseif ( isset( $category ) && $category != "" ) {
				$franchises = FBA_API::get_search_results( '&cid=' . $category );
			} else {
				$franchises = FBA_API::get_franchise_by_id( $franchises, 'minimal' );
			}


			foreach ( $franchises as $franchise ) {
				$count ++;
				if ( $count <= $featured_count ) {
					if ( $display == "name" ) {
						echo "<p><a href='" . home_url( '/franchise/' . $franchise->slug ) . "'>" . esc_attr( $franchise->name ) . "</a></p>";
					} else {
						echo "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "'><img src='" . esc_attr( $franchise->logo ) . "'></a>";
					}
				}

			}

		} else {
			echo "<p>Your Franchise Brokers Association account is currently not active please <a href='http://franchiseba.com/' target='_blank'>contact us</a> to activate your account.";
		}

		echo "</div>";

		echo $after_widget;
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 *
	 * @param array  An array of new settings as submitted by the admin
	 * @param array  An array of the previous settings
	 *
	 * @return array The validated and (if necessary) amended settings
	 **/
	public function update( $new_instance, $old_instance ) {

		// update logic goes here
		$updated_instance = $new_instance;

		return $updated_instance;
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 *
	 * @param array  An array of the current settings for this widget
	 *
	 * @return void Echoes it's output
	 **/
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance );
		extract( $instance );

		//$title = (isset($title)) ? $title : "Featured Franchises";
		$title          = ( isset( $title ) ) ? $title : "Featured Franchises";
		$display        = ( isset( $display ) ) ? $display : "";
		$category       = ( isset( $category ) ) ? $category : "";
		$franchises     = ( isset( $franchises ) ) ? $franchises : [];
		$featured       = ( isset( $featured ) ) ? $featured : "";
		$featured_count = ( isset( $featured_count ) ) ? $featured_count : 5;

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'display' ); ?>"><?php _e( 'Display:' ); ?></label> <br>
			<select class="widefat" name="<?php echo $this->get_field_name( 'display' ); ?>" id="<?php echo $this->get_field_name( 'display' ); ?>">
				<option value="name" <?php echo ( $display == "name" ) ? "selected" : "" ?> >Name</option>
				<option value="logo" <?php echo ( $display == "logo" ) ? "selected" : "" ?> >Logo</option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:' ); ?></label> <br>
			<select class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>" id="<?php echo $this->get_field_name( 'category' ); ?>">
				<option value="">Select One</option>
				<?php
				$categories = FBA_API::get_categories();
				foreach ( $categories as $cat ):
					if ( '' == $cat->name ) {
						continue;
					}
					?>
					<option value="<?php echo $cat->slug; ?>" <?php selected( $cat->slug, $category ) ?>><?php echo $cat->name; ?></option>
				<?php endforeach ?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'featured' ); ?>"><?php _e( 'Featured:' ); ?></label> <br>
			<input type="checkbox" name="<?php echo $this->get_field_name( 'featured' ); ?>" id="<?php echo $this->get_field_name( 'featured' ); ?>" value="true" <?php checked( "true", $featured ); ?>>
			Only Show featured Franchises
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'franchises' ); ?>"><?php _e( 'Franchises:' ); ?></label> <br>
			<select class="widefat" name="<?php echo $this->get_field_name( 'franchises' ); ?>[]" id="<?php echo $this->get_field_name( 'franchises' ); ?>" multiple>

				<?php
				$franchisesList = FBA_API::get_franchises( '&minimal=1' );
				foreach ( $franchisesList as $franchise ): ?>
					<?php if ( in_array( $franchise->id, $franchises ) ): ?>
						<option value="<?php echo $franchise->id; ?>" selected><?php echo esc_attr( $franchise->name ); ?></option>
					<?php else: ?>
						<option value="<?php echo $franchise->id; ?>"><?php echo esc_attr( $franchise->name ); ?></option>
					<?php endif ?>
				<?php endforeach ?>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'featured' ); ?>"><?php _e( 'Franchise Count:' ); ?></label>
			<br>
			<input type="text" name="<?php echo $this->get_field_name( 'featured_count' ); ?>" id="<?php echo $this->get_field_name( 'featured_count' ); ?>" value="<?php echo $featured_count ?>">
		</p>


		<?php

	}
}

add_action( 'widgets_init', function () {
	register_widget( 'FBA_Franchises_Widget' );
} );