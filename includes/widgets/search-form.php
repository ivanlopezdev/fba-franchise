<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class FBA_Form_Widget extends WP_Widget {

	/**
	 * Constructor
	 *
	 * @return void
	 **/
	public function __construct() {

		parent::__construct(
			'fba-form',
			__( 'FBA Search Form', 'fba' ),
			array(
				'classname'   => 'fba-search-class',
				'description' => __( 'Dispaly a franchise search form.', 'fba' )
			)
		);
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array  An array of standard parameters for widgets in this theme
	 * @param array  An array of settings for this widget instance
	 *
	 * @return void Echoes it's output
	 **/
	public function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		echo $before_title;
		echo ( ! empty( $title ) ) ? $title : 'Franchise Search'; // Can set this with a widget option, or omit altogether
		echo $after_title;

		$active = get_transient( 'fba_account_status' );

		if ( ! $active ) {
			$status = FBA_API::get_status();
			$active = $status->broker;
			set_transient( 'fba_account_status', $active, 86400 );
		}

		echo "<div id='search-form-widget'>";


		if ( $active ) {
			?>

			<?php fba_search_form( home_url( 'franchise-search' ) ); ?>
			<?php fba_my_list(); ?>

			<?php
		} else {
			echo "<p>Your Franchise Brokers Association account is currently not active please <a href='http://franchiseba.com/' target='_blank'>contact us</a> to activate your account.";
		}

		echo "</div>";

		echo $after_widget;
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 *
	 * @param array  An array of new settings as submitted by the admin
	 * @param array  An array of the previous settings
	 *
	 * @return array The validated and (if necessary) amended settings
	 **/
	public function update( $new_instance, $old_instance ) {

		// update logic goes here
		$updated_instance = $new_instance;

		return $updated_instance;
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 *
	 * @param array  An array of the current settings for this widget
	 *
	 * @return void Echoes it's output
	 **/
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance );
		extract( $instance );

		$title = ( isset( $title ) ) ? $title : "Franchise Search";

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php

	}
}


add_action( 'widgets_init', function () {
	register_widget( 'FBA_Form_Widget' );
} );

