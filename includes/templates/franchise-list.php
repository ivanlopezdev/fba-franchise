<?php get_header(); ?>


	<article class="recent_search container_12">
		<div class="entry-content " itemprop="text">
			<div class="grid_12">
				<div class="post-heading">
					<h1 class="entry-title" itemprop="headline">Franchise Search</h1>
				</div>
				<!-- .post-heading -->
			</div>

			<?php fba_search_form(); ?>

			<div class="grid_4"></div>
		</div>
	</article>

	<article class="page type-page status-publish entry" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
		<div class="search-results container_12">

			<?php $results = FBA_API::get_search_results( $args ); ?>

			<?php if ( $results ): ?>
				<div class="result gridRow">
					<?php fba_search_header(); ?>
				</div>
				<?php foreach ( $results as $result ): ?>
					<div class="result gridRow">
						<div class="result-image grid_3">
							<?php fba_franchise_listing_logo( $result ) ?>
						</div>
						<div class="result-content grid_9">
							<?php fba_franchise_listing_details( $result ) ?>
						</div>
					</div>
				<?php endforeach ?>

			<?php else: ?>
				<div class="result-error">
					<p>
						<strong>No franchises matched with your selected search criteria. You may want to limit your selections to get better results</strong>.
					</p>>
					<p>
						<strong>OR</strong> take a look at some of these exciting Franchise Business Opportunities that other business owners are taking a look at right now:
					</p>

					<div class='fba-franchise'>
						<?php
						$featured_count = 5;
						$count          = 0;
						$franchises     = FBA_API::get_featured();
						$content        = '';
						foreach ( $franchises as $franchise ) {
							$count ++;
							if ( $count <= $featured_count ) {
								$content .= "<a href='" . home_url( '/franchise/' . $franchise->slug ) . "'><img src='" . esc_attr( $franchise->logo ) . "'></a>";
							}
						}
						echo $content;
						?>
					</div>
				</div>
			<?php endif ?>

			<div style="clear:both"></div>

		</div>
	</article>

<?php get_footer(); ?>