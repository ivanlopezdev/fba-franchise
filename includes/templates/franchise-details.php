<?php function fba_franchise_details_template( $content ) {
	global $wp_query, $post, $franchise, $fba_show_message, $fba_show_details, $fba_api_form;

	if ( $post->post_name == 'franchise' ) {
		ob_start();
		$args = "&";
		if ( isset( $wp_query->query_vars['category'] ) && $wp_query->query_vars['category'] != "" ) {
			$catID = array_search( $wp_query->query_vars['category'], FBA_API::get_categories() );
			$args .= 'cid=' . $catID . '&';
		}
		if ( isset( $wp_query->query_vars['fran-order'] ) && $wp_query->query_vars['fran-order'] != "" ) {
			$args .= 'fran-order=' . strtolower( esc_attr( $wp_query->query_vars['fran-order'] ) );
		}

		$franchise_args = $args;

		?>
		
		<div class="container_12">
			<div class="grid_12">
				<?php fba_search_form( home_url( 'franchise-search' ), false ); ?>
			</div>
		</div>

		<div style="clear:both"></div>

		<div class="result_container">
			<div class="container_12 result-info">


				<div class="grid_8">
					<header class="franchise container_12">

						<div class="grid_6">
							<img src="<?php echo $franchise->logo; ?>">
						</div>
						<div class="grid_6">
							<h2><?php echo $franchise->name ?></h2>

							<p>
								<a href="#territory-check-modal" id="check-territory" class="check-territory" data-franchise="<?php echo $franchise->id ?>::<?php echo $franchise->name ?>">Check Territory</a>
							</p>

							<p>
								<input type="checkbox" data-franchise="<?php echo $franchise->id ?>::<?php echo $franchise->name ?>::<?php echo str_replace( '|', ', ', $franchise->category ) ?>::<?php echo $franchise->average_investment ?>" class="mylist" id="list-<?php echo $franchise->id ?>" />
								<label for="list-<?php echo $franchise->id ?>">Add to My Franchise List</label>
							</p>
						</div>
					</header>

				<?php
				echo wpautop( wptexturize( $franchise->public_description ) );
				?>

<p><i>Please carefully review this disclaimer before considering any franchise opportunities listed on our platform.</i></p>

<p><i>The franchise listings displayed on our website are created and managed by the respective franchisors, who provide all information independently. FBA cannot guarantee the accuracy, reliability, or completeness of any information provided by franchisors. Prospective candidates should treat all information as informational only, without any guarantee of performance, profitability, or returns on investment. We strongly encourage candidates to perform their own comprehensive research and due diligence prior to making any investment decision. FBA recommends that candidates contact the franchisor directly to confirm all details and consider consulting with financial advisors or legal professionals to fully assess the risks associated with a potential investment. FBA disclaims all liability for any decisions or actions taken by candidates in reliance on the information provided in franchise listings. Any decisions made are at the sole risk of the candidate, and FBA cannot be held responsible for any losses, financial or otherwise, that may arise from relying on franchisor-provided information without verification.
By accessing and using the information on this website, the user acknowledges that they understand and agree to this disclaimer.</i></p>
			</div>

			<div class="grid_4">

				<?php dynamic_sidebar( 'franchise-details' ); ?>

				<div class="basic-franchise-info">
					<h4>Basic Franchise Info</h4>
					<?php if ( ! empty( $franchise->average_investment ) && 'N/A' !== $franchise->average_investment ): ?>
						<p>
							<strong>Average Investment: </strong>$<?php echo esc_attr( $franchise->average_investment ); ?>
						</p>
					<?php else: ?>
						<p></p><strong>Average Investment: </strong> N/A</p>
					<?php endif; ?>
					<?php if ( ! empty( $franchise->minimum_investment ) && 'N/A' !== $franchise->minimum_investment ): ?>
						<p>
							<strong>Minimum Investment: </strong>$<?php echo esc_attr( $franchise->minimum_investment ); ?>
						</p>
					<?php else: ?>
						<p><strong>Minimum Investment: </strong> N/A</p>
					<?php endif; ?>

					<div class="stats ">
						<?php if ( $fba_api_form && ! $fba_show_details ): ?>
							<div class="details-prompts">
								To see this information click the button below!
							</div>
						<?php endif; ?>
						<div class="extra-details <?php echo ( $fba_show_details ) ? '' : "hide-details" ?>" oncopy="return false" oncut="return false" onpaste="return false">
							<p><strong>Min. Liquidity:</strong> <?php echo esc_attr( $franchise->liquidity ); ?></p>

							<p>
								<strong>Years in Business:</strong> <?php echo esc_attr( $franchise->year_established ); ?>
							</p>

							<p><strong>Open Units:</strong> <?php echo esc_attr( $franchise->franchised_units ); ?></p>

							<p>
								<strong>In-House Financing:</strong> <?php echo ! empty( $franchise->financial_assistance ) ? 'Yes' : 'No'; ?>
							</p>

							<p>
								<strong>Lead Assist:</strong> <?php echo ! empty( $franchise->lead_assistance ) ? 'Yes' : 'No'; ?>
							</p>

							<p>
								<strong>Coaching:</strong> <?php echo ! empty( $franchise->financial_assistance ) ? 'Yes' : 'No' ?>
							</p>
						</div>

					</div>

					<?php if ( isset( $fba_api_form ) && $fba_api_form && ! $fba_show_details ): ?>
						<a href="#request-information-modal" class="request-information" data-franchise="<?php echo $franchise->id ?>::<?php echo $franchise->name; ?>">Request Information</a>
					<?php endif ?>

				</div>
			</div>

			<div style="display: none">
				<div id="request-information-modal">
					<h4>Request More Information on <span class="franchise-name"><?php echo $franchise->name ?></span>
					</h4>

					<?php $fba_settings = (array) get_option( 'fba_setting' ); ?>
					<?php if( ! empty( $fba_settings['request_information'] ) ): ?>
						<div class="grid_5 fba-gravity-form" data-gf="<?php echo $fba_settings['request_information'] ?>">
							<?php if(function_exists('gravity_form')){
								gravity_form( $fba_settings['request_information'], false, false, false, '', true );
							} ?>
						</div>
					<?php else: ?>
						<div class="form_message"></div>
						<form action="" method="post" name="details_information" id="fba-details-request">
						<div class="grid_5">
								<div class="form_field">
									<input type="text" name="first_name" placeholder="First Name">
								</div>
								<div class="form_field">
									<input type="text" name="last_name" placeholder="Last Name">
								</div>
								<div class="form_field">
									<input type="text" name="email" placeholder="Email">
								</div>
								<div class="form_field">
									<input type="text" name="phone" placeholder="Phone Number">
								</div>
								<div class="form_field">
									<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
								</div>
								<div class="btn_field">
									<input type="submit" name="submit" value="Get More Info!">
								</div>

						</div>
						<div class="grid_7">
							<textarea name="comment" cols="30" rows="10" placeholder="Comments and Questions"></textarea>
							<input type="hidden" name="action_type" value="franchise">
						</div>
						</form>
					<?php endif; ?>
					
					<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>
					
				</div>
			</div>

			<div style="display: none">
				<div id="territory-check-modal">
					<h4>Territory Check on <span class="franchise-name"></span></h4>

					<div class="form_message"></div>
					<form action="" method="post" name="territory_information" id="fba-territory-request">

						<div class="grid_5">
							<div class="form_field">
								<input type="text" name="first_name" placeholder="First Name">
							</div>
							<div class="form_field">
								<input type="text" name="last_name" placeholder="Last Name">
							</div>
							<div class="form_field">
								<input type="text" name="email" placeholder="Email">
							</div>
							<div class="form_field">
								<input type="text" name="phone" placeholder="Phone Number">
							</div>
							<div class="form_field">
								<input type="text" name="validator" id="validator" class="validator" placeholder="Are you human?">
							</div>
							<div class="btn_field">
								<input type="hidden" name="action_type" value="territory">
								<input type="submit" name="submit" value="Get More Info!">
							</div>

						</div>

						<div class="grid_7">
							<div class="form_field">
								<textarea name="territories" cols="30" rows="2" placeholder="What territories are your interested in?"></textarea>
							</div>
							<div class="form_field">
								<input type="text" name="timeframe" id="timeframe" placeholder="What is your timeframe to purchase?">
							</div>
							<div class="form_field">
								<input type="text" name="invest" id="invest" placeholder="How much do you have to invest?">
							</div>
							<div class="form_field">
								<strong>What is the best time to call you:</strong> <br />
								<input type="radio" name="time" value="Morning"> Morning
								<input type="radio" name="time" value="Afternoon"> Afternoon
								<input type="radio" name="time" value="Evenings"> Evenings
							</div>
						</div>
					</form>

					<div class="form-disclaimer">By submitting the form, you consent to receiving communications via calls, text messages, or emails from us at the contact details provided. Standard message and data rates may apply. You can opt out of text messaging by texting STOP at any time. Please refer to our Terms of Service and Privacy Policy for further information.</div>

				</div>
			</div>

			<div style="clear:both"></div>

			
		</div>

		
		</div>
		<?php
		$content = ob_get_clean();
	}

	return $content;
}