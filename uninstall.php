<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   FBA Franchises
 * @author    Ivan Lopez <ivan@wpconsole.com>
 * @license   GPL-2.0+
 * @link      http://wpconsole.com
 * @copyright 2013 WP Console LLC
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// @TODO: Define uninstall functionality here